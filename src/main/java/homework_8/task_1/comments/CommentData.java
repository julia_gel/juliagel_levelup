package homework_8.task_1.comments;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class CommentData {

    private int id;
    private int post_id;
    private String name;
    private String email;
    private String body;
    @SerializedName("created_at")
    private String createDate;
    @SerializedName("updated_at")
    private String updateDate;

    public CommentData(int id, int post_id, String name, String email, String body, String createDate, String updateDate) {
        this.id = id;
        this.post_id = post_id;
        this.name = name;
        this.email = email;
        this.body = body;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public int getId() {
        return id;
    }

    public int getPost_id() {
        return post_id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getBody() {
        return body;
    }

    public String getCreateDate() {
        return createDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    @Override
    public String toString() {
        return "CommentData{" +
                "id=" + id +
                ", post_id=" + post_id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", body='" + body + '\'' +
                ", createDate='" + createDate + '\'' +
                ", updateDate='" + updateDate + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommentData that = (CommentData) o;
        return id == that.id &&
                post_id == that.post_id &&
                Objects.equals(name, that.name) &&
                Objects.equals(email, that.email) &&
                Objects.equals(body, that.body) &&
                Objects.equals(createDate, that.createDate) &&
                Objects.equals(updateDate, that.updateDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, post_id, name, email, body, createDate, updateDate);
    }
}
