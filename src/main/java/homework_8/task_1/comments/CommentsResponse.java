package homework_8.task_1.comments;

import homework_8.task_1.Meta;

import java.util.Objects;

public class CommentsResponse {
    private int code;
    private Meta meta;
    private CommentData data;

    public CommentsResponse() {
    }

    public CommentsResponse(int code, Meta meta, CommentData data) {
        this.code = code;
        this.meta = meta;
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public Meta getMeta() {
        return meta;
    }

    public CommentData getData() {
        return data;
    }

    @Override
    public String toString() {
        return "PostsResponse{" +
                "code=" + code +
                ", meta=" + meta +
                ", data=" + data +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommentsResponse that = (CommentsResponse) o;
        return code == that.code &&
                Objects.equals(meta, that.meta) &&
                Objects.equals(data, that.data);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, meta, data);
    }
}
