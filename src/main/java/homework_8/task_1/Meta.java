package homework_8.task_1;

import java.util.Objects;

public class Meta {
    private Pagination pagination;

    public Meta() {}

    public Meta(Pagination pagination) {
        this.pagination = pagination;
    }

    public Pagination getPagination() {
        return pagination;
    }

    @Override
    public String toString() {
        return "Meta{" +
                "pagination=" + pagination +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Meta meta = (Meta) o;
        return Objects.equals(pagination, meta.pagination);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pagination);
    }
}
