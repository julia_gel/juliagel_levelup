package homework_8.task_1;

import java.util.Objects;

public class Pagination {
    private int total;
    private int pages;
    private int page;
    private int limit;

    public Pagination() {
    }

    public Pagination(int total, int pages, int page, int limit) {
        this.total = total;
        this.pages = pages;
        this.page = page;
        this.limit = limit;
    }

    public int getTotal() {
        return total;
    }

    public int getPages() {
        return pages;
    }

    public int getPage() {
        return page;
    }

    public int getLimit() {
        return limit;
    }

    @Override
    public String toString() {
        return "Pagination{" +
                "total=" + total +
                ", pages=" + pages +
                ", page=" + page +
                ", limit=" + limit +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pagination that = (Pagination) o;
        return total == that.total &&
                pages == that.pages &&
                page == that.page &&
                limit == that.limit;
    }

    @Override
    public int hashCode() {
        return Objects.hash(total, pages, page, limit);
    }
}
