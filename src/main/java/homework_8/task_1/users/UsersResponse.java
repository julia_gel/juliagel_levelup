package homework_8.task_1.users;

import homework_8.task_1.Meta;

import java.util.List;
import java.util.Map;
import java.util.Objects;

public class UsersResponse {
    private int code;
    private Meta meta;
    private UserData data;

    public UsersResponse() {
    }

    public UsersResponse(int code, Meta meta, UserData data) {
        this.code = code;
        this.meta = meta;
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public Meta getMeta() {
        return meta;
    }

    public UserData getData() {
        return data;
    }

    @Override
    public String toString() {
        return "UsersResponse{" +
                "code=" + code +
                ", meta=" + meta +
                ", data=" + data +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UsersResponse that = (UsersResponse) o;
        return code == that.code &&
                Objects.equals(meta, that.meta) &&
                Objects.equals(data, that.data);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, meta, data);
    }
}
