package homework_8.task_1.users;

import java.util.Objects;

public class UsersRequest {
    private String name;
    private String gender;
    private String email;
    private String status;

    public UsersRequest(String name, String gender, String email, String status) {

        this.name = name;
        this.gender = gender;
        this.email = email;
        this.status = status;
    }


    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    public String getEmail() {
        return email;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "UsersRequest{" +
                "name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", email='" + email + '\'' +
                ", status='" + status + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UsersRequest that = (UsersRequest) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(gender, that.gender) &&
                Objects.equals(email, that.email) &&
                Objects.equals(status, that.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, gender, email, status);
    }
}
