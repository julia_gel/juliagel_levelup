package homework_8.task_1.posts;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class PostData {

    private int id;
    private int user_id;
    private String title;
    private String body;
    @SerializedName("created_at")
    private String createDate;
    @SerializedName("updated_at")
    private String updateDate;

    public PostData(int id, int user_id, String title, String body, String createDate, String updateDate) {
        this.id = id;
        this.user_id = user_id;
        this.title = title;
        this.body = body;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public int getId() {
        return id;
    }

    public int getUser_id() {
        return user_id;
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }

    public String getCreateDate() {
        return createDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    @Override
    public String toString() {
        return "PostData{" +
                "id=" + id +
                ", user_id=" + user_id +
                ", title='" + title + '\'' +
                ", body='" + body + '\'' +
                ", createDate='" + createDate + '\'' +
                ", updateDate='" + updateDate + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PostData postData = (PostData) o;
        return id == postData.id &&
                user_id == postData.user_id &&
                Objects.equals(title, postData.title) &&
                Objects.equals(body, postData.body) &&
                Objects.equals(createDate, postData.createDate) &&
                Objects.equals(updateDate, postData.updateDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, user_id, title, body, createDate, updateDate);
    }
}
