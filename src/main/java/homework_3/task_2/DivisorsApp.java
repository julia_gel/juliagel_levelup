package homework_3.task_2;

import java.util.*;

public class DivisorsApp {
    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<>();
        int n = 100000;

        for (int i = 0; i <= n; i++) {
            a.add(i);
        }

        Collections.shuffle(a);

        //System.out.println(a);

        /*Проверяем на уникальность элементы коллекции а*/
        Set<Integer> set = new HashSet<Integer>(a);
        if(set.size() == a.size()){
            System.out.println("There are no duplicates");
        } else {
            System.out.println("There are duplicates");
        }

        /*Проверяем делится ли элемент на 2,3,5,7*/
        HashMap<Integer, List<Integer>> map = new HashMap<>();
        map.put(2, new ArrayList<>());
        map.put(3, new ArrayList<>());
        map.put(5, new ArrayList<>());
        map.put(7, new ArrayList<>());

        for (Integer i:a) {
            if(i%2==0){
                map.get(2).add(i);
                continue; //для того чтобы элемент попадал только в первый список, еслси элемент делится на несколько делителей
            }
            if(i%3==0){
                map.get(3).add(i);
                continue;
            }
            if(i%5==0){
                map.get(5).add(i);
                continue;
            }
            if(i%7==0){
                map.get(7).add(i);
                continue;
            }
        }

        /*Получение по делителю всех элементов из коллекции, которые на него делятся*/
        System.out.println(map.get(7));
    }
}
