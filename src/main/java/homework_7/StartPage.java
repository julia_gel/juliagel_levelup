package homework_7;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import static com.codeborne.selenide.Condition.*;

import static com.codeborne.selenide.Selenide.*;

public class StartPage {

    SelenideElement userLoginButton = $(By.className("button2_theme_mail-white"));
    SelenideElement startPageTitle = $x("//title");

    @Step("Открытие стартовой страницы Яндекс почты")
    public void openYandexStartPage(String url){

        Selenide.open(url);
        System.out.println(Selenide.title());
        System.out.println(startPageTitle.getText());

    }

    @Step("Логин")
    public void clickLoginButton(){
        userLoginButton.shouldBe(visible).click();

    }
}
