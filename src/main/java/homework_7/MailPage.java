package homework_7;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;

public class MailPage  {

    SelenideElement mailSubject = $x("//div[contains(@class,'mail-Message-Toolbar-Subject mail-Message-Toolbar-Subject_message')]");

    SelenideElement mailBody = $x("//div[contains(@class, 'mail-Message-Body-Content')]/div");

    SelenideElement mailSender = $("span.mail-Message-Sender-Email");

    SelenideElement deleteButton = $x("//div[contains(@class,'ns-view-toolbar-button-delete')]");


    @Step("Проверка темы письма")
    public String getMailSubject(){

        return mailSubject.shouldBe(Condition.visible).getText();
    }

    @Step("Проверка тела письма")
    public String getMailBody(){

        return mailBody.shouldBe(Condition.visible).getText();
    }

    @Step("Проверка отправителя письма")
    public String getMailSender(){

        return mailSender.shouldBe(Condition.visible).getText();
    }

    @Step("Удаление письма")
    public void deleteMail(){
        deleteButton.shouldBe(Condition.visible).click();
    }
}
