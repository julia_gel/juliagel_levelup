package homework_7;


import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.*;


import static com.codeborne.selenide.Selenide.*;


public class HomePage {

    SelenideElement composeNewMailButton = $(By.className("mail-ComposeButton"));

    SelenideElement openInboxFolderButton = $("[title*='Входящие']");

    SelenideElement openDraftFolderButton = $("[data-title='Черновики']");

    SelenideElement openSentFolderButton = $("[data-title='Отправленные']");

    SelenideElement openTestFolderButton = $("[data-title='Test']");

    SelenideElement openDeletedFolderButton = $("[data-title='Удалённые']");

    SelenideElement mailItemSubject = $x("//span[contains(text(),'\" + mailSubjectText + \"')]");

    SelenideElement messagesPlaceholderEmpty = $("div.b-messages__placeholder-item:first-child");

    SelenideElement accountMenu = $x("//a[@href=\"https://passport.yandex.ru\"]");

    SelenideElement logoutButton = $("a[aria-label='Выйти из аккаунта']");

    SelenideElement loggedInAccount = $x("//a[@href=\"https://passport.yandex.ru\"]/span[@class='user-account__name']");

    SelenideElement currentFolder = $("a.mail-NestedList-Item_current");

    SelenideElement refreshButton = $("span.mail-ComposeButton-Refresh");

    SelenideElement replyTextField = $("div.mail-QuickReply-Placeholder_text");

@Step("Открывается папка Входящие")
    public void openInboxFolder() {
        openInboxFolderButton.shouldBe(Condition.visible).click();
        currentFolder.shouldBe(Condition.attributeMatching("data-title", "Входящие"));
        refreshButton.click();
    }

    @Step("Создается новое письмо")
    public void composeNewMail() {
        composeNewMailButton.shouldBe(Condition.visible).click();
    }

    @Step("Открывается папка Черновики")
    public void openDraftFolder() {

        openDraftFolderButton.shouldBe(Condition.visible).click();
        currentFolder.shouldBe(Condition.attributeMatching("data-title", "Черновики"));

    }
@Step("Открываем письмо")
    public void openMailBySubject(String mailSubjectText) {
        $x("//span[contains(text(),'" + mailSubjectText + "')]").click();

    }
@Step("Поиск письма по теме письма '{0}'")
    public SelenideElement findMailBySubject(String mailSubjectText) {
        actions().sendKeys(Keys.F9).build().perform();
        return $("span[title='" + mailSubjectText + "']").shouldBe(Condition.visible);
    }

    @Step("Проверка что папка пустая")
    public String isFolderEmpty() {
        return messagesPlaceholderEmpty.shouldBe(Condition.visible).getText();
    }

    @Step("Открывается папка Отправленные")
    public void openSentFolder() {
        openSentFolderButton.shouldBe(Condition.visible).click();
        currentFolder.shouldBe(Condition.attributeMatching("data-title", "Отправленные"));

    }

    @Step("Логаут")
    public void logout() {
        accountMenu.shouldBe(Condition.visible).click();
        logoutButton.shouldBe(Condition.visible).click();
    }

    @Step("Проверка аккаунта под которым выполнен вход")
    public String getLoggedInUserAccount() {
        return loggedInAccount.shouldBe(Condition.visible).getText();
    }

    @Step("Открывается папка Тест")
    public void openTestFolder() {

        openTestFolderButton.shouldBe(Condition.visible).click();
        currentFolder.shouldBe(Condition.attributeMatching("data-title", "Test"));

    }

    @Step("Открывается папка Удаленные")
    public void openDeletedFolder() {
        openDeletedFolderButton.shouldBe(Condition.visible).click();
        currentFolder.shouldBe(Condition.attributeMatching("data-title", "Удалённые"));
    }

    @Step("Открывается письмо из треда")
    public void openMailInThread(String mailSubjectText, String mailBodyText) {
        $x("//span[contains(text(),'" + mailSubjectText + "')]").shouldBe(Condition.visible).click();
        $$x("//div[contains(@class,'mail-MessageSnippet-Inner mail-MessageSnippet-Thread')]//span[contains(text(),'" + mailBodyText + "')]").get(1).shouldBe(Condition.visible).click();
        replyTextField.shouldBe(Condition.visible);
    }
}

