package homework_7;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static com.codeborne.selenide.Selenide.$;
import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;

public class LoginPage  {

    SelenideElement userNameTextField = $(By.id("passp-field-login"));

    SelenideElement userPasswordTextField = $(By.id("passp-field-passwd"));

    SelenideElement loginSubmitButton = $(By.className("Button2_type_submit"));

@Step("Ввод логина и пароля ")
    public void login(String login, String password){
        /*wait.until(elementToBeClickable(userNameTextField)).sendKeys(login);
        wait.until(elementToBeClickable(loginSubmitButton)).submit();
        wait.until(elementToBeClickable(userPasswordTextField)).sendKeys(password);
        wait.until(elementToBeClickable(loginSubmitButton)).submit();*/
        userNameTextField.shouldBe(Condition.visible).sendKeys(login);
        loginSubmitButton.shouldBe(Condition.visible).click();
        userPasswordTextField.shouldBe(Condition.visible).sendKeys(password);
        loginSubmitButton.shouldBe(Condition.visible).click();



    }
}
