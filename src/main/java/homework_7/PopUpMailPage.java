package homework_7;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.*;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.*;

public class PopUpMailPage  {

    SelenideElement mailNewHeader = $x("//span[@class='composeHeader-Title']/span");

    SelenideElement mailDraftHeader = $x("//span[@class='composeHeader-Title']/span");

    SelenideElement mailToTextField = $x("//div[@class='MultipleAddressesDesktop-Field ComposeYabblesField']/div");

    SelenideElement mailSubjectTextField = $(By.name("subject"));

    SelenideElement mailBodyTextField = $x("//div[contains(@class, 'cke_editable_themed cke_contents_ltr')]/div");

    SelenideElement savedMailLabel = $x("//span[@class='composeHeader-SavedAt']");

    SelenideElement closeButton = $x("//div[@class='ComposePopup-Content']//button[contains(@class, 'controlButtons__btn--close')]");

    SelenideElement recepient = $("span[data-email]");

    SelenideElement sendButton = $x("//div[@class='ComposeSendButton-Text']/../..");

    SelenideElement composeDoneScreen = $("div.ComposeDoneScreen-Title span");

    @Step("Ввод адресата")
    public void enterMailTo(String mailToAddress){
        mailToTextField.shouldBe(Condition.visible).sendKeys(mailToAddress);
    }

    @Step("Ввод темы письма")
    public void enterMailSubject(String mailSubjectText){
        mailSubjectTextField.shouldBe(Condition.visible).sendKeys(mailSubjectText);
    }

    @Step("Ввод тела письма")
    public void enterMailBody(String mailBodyText){
        mailBodyTextField.shouldBe(Condition.visible).sendKeys(mailBodyText);
    }

    @Step("Сохранение черновика")
    public void saveDraftMail()
    {
        actions().keyDown(Keys.CONTROL).sendKeys("s").build().perform();
        actions().keyUp(Keys.CONTROL).sendKeys(Keys.NULL).build().perform();
    }

    @Step("Проверка, что письмо сохранено")
    public String isMailSaved()
    {
      return savedMailLabel.shouldBe(Condition.visible).getText();
    }

    @Step("Закрытие письма")
    public void closeMail()
    {
        closeButton.shouldBe(Condition.visible).click();
    }

    @Step("Проверка тела письма")
    public String getMailBody(){
        return  mailBodyTextField.shouldBe(Condition.visible).text();
    }

    @Step("Проверка получателя письма")
    public String getMailRecepient() {
        return recepient.shouldBe(Condition.visible).getAttribute("data-email");
    }

    @Step("Проверка темы письма")
    public String getMailSubject(){
        return mailDraftHeader.shouldBe(Condition.visible).getText();
    }

    @Step("Отправка письма")
    public void sendMail()
    {
        sendButton.shouldBe(Condition.visible).click();
    }

    @Step("Проверка, что письмо отправлено")
    public boolean isMailSent(){
        return composeDoneScreen.shouldBe(Condition.visible).getText().contains("Письмо отправлено");

    }

}
