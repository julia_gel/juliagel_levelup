package homework_2.task_1.products.comparators;

import homework_2.task_1.products.Product;

import java.util.Comparator;

public class SortProductsByCaloriesComparator implements Comparator<Product> {
    @Override
    public int compare(Product p1, Product p2) {
        if (p1.getCalories() > p2.getCalories()) {
            return 1;
        }
        if (p1.getCalories() < p2.getCalories()) {
            return -1;
        }
        return 0;
    }
}
