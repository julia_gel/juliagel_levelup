package homework_2.task_1.products.comparators;

import homework_2.task_1.products.Product;

import java.util.Comparator;

public class SortProductsByExpirationDateComparator implements Comparator<Product> {
    @Override
    public int compare(Product p1, Product p2) {
        if (p1.getExpirationDate().after(p2.getExpirationDate())) {
            return 1;
        }
        if (p1.getExpirationDate().before(p2.getExpirationDate())) {
            return -1;
        }
        return 0;
    }
}
