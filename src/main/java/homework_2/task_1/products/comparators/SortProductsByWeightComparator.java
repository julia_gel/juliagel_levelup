package homework_2.task_1.products.comparators;

import homework_2.task_1.products.Product;

import java.util.Comparator;

public class SortProductsByWeightComparator implements Comparator<Product> {
    @Override
    public int compare(Product p1, Product p2) {
        if (p1.getWeight() > p2.getWeight()) {
            return 1;
        }
        if (p1.getWeight() < p2.getWeight()) {
            return -1;
        }
        return 0;
    }
}
