package homework_2.task_1.products;

import java.util.Date;

public class Beverage extends Product {

    public Beverage(int calories, int weight, Date expirationDate, String name) {
        super(calories, weight, expirationDate, name);
    }
}
