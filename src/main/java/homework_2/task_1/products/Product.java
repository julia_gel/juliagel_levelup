package homework_2.task_1.products;

import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class Product {
    protected Date expirationDate;
    protected int calories;
    protected int weight;
    protected String name;

    public Product(int calories, int weight, Date expirationDate, String name) {
        this.calories = calories;
        this.weight = weight;
        this.expirationDate = expirationDate;
        this.name = name;
    }


    public int getCalories() {
        return calories;
    }

    public int getWeight() {
        return weight;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public String getStringRepresentationOfExpirationDate() {
        return new SimpleDateFormat("dd/MM/yyyy").format(expirationDate);
    }

    public String getName() {
        return name;
    }
}
