package homework_2.task_1;

import homework_2.task_1.products.Product;
import homework_2.task_1.products.Vegetable;
import homework_2.task_1.products.comparators.SortProductsByCaloriesComparator;
import homework_2.task_1.products.comparators.SortProductsByExpirationDateComparator;
import homework_2.task_1.products.comparators.SortProductsByWeightComparator;

import java.util.ArrayList;
import java.util.List;

public class Refrigerator {
    private List<Product> coldProducts = new ArrayList<>();

    public List<Product> getColdProducts() {
        return coldProducts;
    }

    public void put(Product product) {
        if (!this.coldProducts.contains(product)) {
            coldProducts.add(product);
        }
    }

    public void sortByWeight() {
        coldProducts.sort(new SortProductsByWeightComparator());
    }

    public void sortByExpirationDate() {
        coldProducts.sort(new SortProductsByExpirationDateComparator());
    }

    public void sortByCalories() {
        coldProducts.sort(new SortProductsByCaloriesComparator());
    }

    public List<Product> filterVegetables() {
        List<Product> result = new ArrayList<>();
        for (Product product : coldProducts) {
            if (product instanceof Vegetable) {
                result.add(product);
            }
        }
        return result;
    }


    public void printContent() {
        ProductPrintHelper.printContent("В холодильнике есть:", this.coldProducts);
    }

}
