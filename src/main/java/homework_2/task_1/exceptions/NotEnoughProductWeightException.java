package homework_2.task_1.exceptions;

public class NotEnoughProductWeightException extends Exception{
    public NotEnoughProductWeightException(String message) {
        super(message);
    }
}
