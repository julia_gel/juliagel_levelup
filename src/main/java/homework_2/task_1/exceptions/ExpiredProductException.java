package homework_2.task_1.exceptions;

public class ExpiredProductException extends Exception {
    public ExpiredProductException(String message) {
        super(message);
    }
}
