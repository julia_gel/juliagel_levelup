package homework_2.task_1.recepies;

import homework_2.task_1.exceptions.ExpiredProductException;
import homework_2.task_1.exceptions.NotEnoughProductWeightException;
import homework_2.task_1.products.Vegetable;

import java.util.ArrayList;
import java.util.Arrays;

public class FriedPotato extends Receipt {
    private Vegetable potato;

    public FriedPotato(Vegetable potato) {
        this.name = "Жареная картошка";
        this.potato = potato;
        setIngredients(new ArrayList<>(Arrays.asList(potato)));
    }

    @Override
    public void cook() throws ExpiredProductException, NotEnoughProductWeightException {
        System.out.println("Let's cook fried potato");
        this.validateIngredients();
    }

    @Override
    protected void validateIngredients() throws ExpiredProductException, NotEnoughProductWeightException{
        super.validateIngredients();
            if (potato.getWeight() < 1000) {
                throw new NotEnoughProductWeightException("Да тут нечего жарить");
            }
        }
    }

