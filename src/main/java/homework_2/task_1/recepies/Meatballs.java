package homework_2.task_1.recepies;

import homework_2.task_1.exceptions.ExpiredProductException;
import homework_2.task_1.exceptions.NotEnoughProductWeightException;
import homework_2.task_1.products.Meat;
import homework_2.task_1.products.Vegetable;

import java.util.ArrayList;
import java.util.Arrays;

public class Meatballs extends Receipt {

    private Meat beef;
    private Vegetable carrot;

    public Meatballs(Meat beef, Vegetable carrot) {
        this.name = "Фрикадельки";
        this.beef = beef;
        this.carrot = carrot;
        setIngredients(new ArrayList<>(Arrays.asList(beef, carrot)));
    }

    @Override
    public void cook() throws ExpiredProductException, NotEnoughProductWeightException {
        System.out.println("Lets cook meatballs");
        this.validateIngredients();
    }

    @Override
    protected void validateIngredients() throws ExpiredProductException, NotEnoughProductWeightException{
        super.validateIngredients();
            if (beef.getWeight() < 300) {
                throw new NotEnoughProductWeightException("Для фрикаделек нужно больше мяса");
            }
    }
}
