package homework_2.task_1.recepies;

import homework_2.task_1.exceptions.ExpiredProductException;
import homework_2.task_1.exceptions.NotEnoughProductWeightException;
import homework_2.task_1.products.Meat;
import homework_2.task_1.products.Vegetable;

import java.util.ArrayList;
import java.util.Arrays;

public class Borsch extends Receipt {

    private Meat beef;
    private Vegetable potato;
    private Vegetable beet;
    private Vegetable carrot;

    public Borsch(Meat beef, Vegetable potato, Vegetable beet, Vegetable carrot) {
        this.name = "Борщ";
        this.beef = beef;
        this.potato = potato;
        this.beet = beet;
        this.carrot = carrot;
        setIngredients(new ArrayList<>(Arrays.asList(beef, potato, beet, carrot)));

    }

    @Override
    public void cook() throws ExpiredProductException, NotEnoughProductWeightException{
        System.out.println("Приготовим борщ");
        this.validateIngredients();
    }

    @Override
    protected void validateIngredients() throws ExpiredProductException, NotEnoughProductWeightException {
            super.validateIngredients();
                if (beef.getWeight() < 100) {
                    throw new NotEnoughProductWeightException("Слишком мало говядины для борща");
                }
                if (beet.getWeight() < 100) {
                    throw new NotEnoughProductWeightException("Слишком мало свеклы для борща");
                }
            }


        }




