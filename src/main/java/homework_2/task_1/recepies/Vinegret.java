package homework_2.task_1.recepies;

import homework_2.task_1.exceptions.ExpiredProductException;
import homework_2.task_1.exceptions.NotEnoughProductWeightException;
import homework_2.task_1.products.Vegetable;

import java.util.ArrayList;
import java.util.Arrays;

public class Vinegret extends Receipt {
    Vegetable potato;
    Vegetable beet;
    Vegetable carrot;

    public Vinegret(Vegetable potato, Vegetable beet, Vegetable carrot) {
        this.name = "Винегрет";
        this.potato = potato;
        this.beet = beet;
        this.carrot = carrot;
        setIngredients(new ArrayList<>(Arrays.asList(potato, beet, carrot)));
    }

    @Override
    public void cook() throws ExpiredProductException, NotEnoughProductWeightException{
        System.out.println("Lets cook vinegret");
        this.validateIngredients();
    }

    @Override
    protected void validateIngredients() throws ExpiredProductException, NotEnoughProductWeightException {
        super.validateIngredients();
        if (potato.getWeight() < 100) {
            throw new NotEnoughProductWeightException("Слишком мало картошки для винегрета");
            }
        if (beet.getWeight() < 100) {
            throw new NotEnoughProductWeightException("Слишком мало свеклы для винегрета");
            }
        }
    }

