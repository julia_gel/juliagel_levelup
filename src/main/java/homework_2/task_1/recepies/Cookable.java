package homework_2.task_1.recepies;

import homework_2.task_1.exceptions.ExpiredProductException;
import homework_2.task_1.exceptions.NotEnoughProductWeightException;

public interface Cookable {
    void cook() throws ExpiredProductException, NotEnoughProductWeightException;
}
