package homework_2.task_1.recepies;

import homework_2.task_1.exceptions.ExpiredProductException;
import homework_2.task_1.exceptions.NotEnoughProductWeightException;
import homework_2.task_1.exceptions.ProductNotFoundException;
import homework_2.task_1.products.Product;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public abstract class Receipt implements Cookable, CalloriesAware {
    List<Product> ingredients = new ArrayList<>();
    String name;


    public void setIngredients(List<Product> ingredients) {
        this.ingredients.addAll(ingredients);
    }

    public int calculateCalories() {
        int result = 0;
        int ingredientCalories = 0;
        int overallWeight = 0;

        for (Product product : this.ingredients) {
            ingredientCalories = product.getCalories() * product.getWeight() / 100;
            overallWeight += product.getWeight();
            result += Math.round(ingredientCalories);
        }
        System.out.println(String.format("Калорийность %s  %d ккал в 100 гр", this.name, (result * 100) / overallWeight));
        System.out.println(String.format("Общая калорийность %d гр %s %d", overallWeight, this.name, result));
        return result;
    }

    protected void validateIngredients() throws ExpiredProductException, NotEnoughProductWeightException {
        System.out.println("Проверяем сроки годности и достаточно ли ингридиентов для готовки");
        for (Product product : ingredients) {
            if (product.getExpirationDate().before(Calendar.getInstance().getTime())) {
                throw new ExpiredProductException("Просроченный продукт");
            }
        }
    }

    public Product findProductByWeight(int minWeight, int maxWeight) throws ProductNotFoundException {
        for (Product product : ingredients) {
            if (maxWeight >= product.getWeight() && minWeight <= product.getWeight()) {
                return product;
            }
        }
        throw new ProductNotFoundException("Продукт, подходящий под заданные критерии, не найден");
    }

}
