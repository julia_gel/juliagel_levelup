package homework_2.task_1;

import homework_2.task_1.products.Product;

import java.util.List;

public class ProductPrintHelper {
    public static void printContent(String message, List<Product> products) {
        System.out.println(message);
        for (Product product : products) {
            printProduct(product);
        }
    }

    public static void printProduct(Product product) {
        if (product == null) {
            return;
        }
        System.out.println(String.format("%s %d %d %s",
                product.getName(),
                product.getWeight(),
                product.getCalories(),
                product.getStringRepresentationOfExpirationDate()));
    }
}
