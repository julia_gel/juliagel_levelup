package homework_2.task_1;

import homework_2.task_1.recepies.Receipt;

import java.util.Date;
import java.util.List;

public class Menu {

    private Date date;
    private List<Receipt> recepies;

    public void setRecepies(List<Receipt> recepies) {
        this.recepies = recepies;
    }

    public int calculateCalories() {
        int result = 0;
        for (Receipt receipt : this.recepies) {
            result += receipt.calculateCalories();
        }
        System.out.println(String.format("Общая калорийность меню %d ккал", result));
        return result;
    }

}
