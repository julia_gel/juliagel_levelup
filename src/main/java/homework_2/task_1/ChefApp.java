package homework_2.task_1;

import homework_2.task_1.exceptions.ExpiredProductException;
import homework_2.task_1.exceptions.NotEnoughProductWeightException;
import homework_2.task_1.exceptions.ProductNotFoundException;
import homework_2.task_1.products.Fish;
import homework_2.task_1.products.Meat;
import homework_2.task_1.products.Product;
import homework_2.task_1.products.Vegetable;
import homework_2.task_1.products.comparators.SortProductsByWeightComparator;
import homework_2.task_1.recepies.*;

import java.util.*;

public class ChefApp {
    public static void main(String[] args) {
        ChefApp myChefApp = new ChefApp();
        myChefApp.startChefApp();
    }

    public void startChefApp() {
        Kitchen myKitchen = new Kitchen();
        Refrigerator myRefrigerator = new Refrigerator();
        Menu myMenu = new Menu();
        myKitchen.setRefrigerator(myRefrigerator);
        myKitchen.setMenu(myMenu);

        /*Создаем экземпляры необходимых продуктов*/
        Fish salmon = new Fish(208, 400, new GregorianCalendar(2020, Calendar.JULY, 11).getTime(), "Лосось");
        Meat beef = new Meat(250, 600, new GregorianCalendar(2020, Calendar.JUNE, 3).getTime(), "Говядина");
        Vegetable potato = new Vegetable(77, 500, new GregorianCalendar(2020, Calendar.JULY, 4).getTime(), "Картофель");
        Vegetable carrot = new Vegetable(32, 100, new GregorianCalendar(2020, Calendar.JULY, 1).getTime(), "Морковь");
        Vegetable beet = new Vegetable(43, 200, new GregorianCalendar(2019, Calendar.JULY, 10).getTime(), "Свекла старая");
        Vegetable beet2 = new Vegetable(43, 200, new GregorianCalendar(2020, Calendar.JULY, 10).getTime(), "Свекла новая");

        /*Кладем их в холодильник*/
        myRefrigerator.put(salmon);
        myRefrigerator.put(beef);
        myRefrigerator.put(potato);
        myRefrigerator.put(carrot);
        myRefrigerator.put(beet);
        myRefrigerator.put(beet2);

        /*Вывести неотсортированное содержимое холодильника*/
        myRefrigerator.printContent();

        /*Отсортировать содержимое по весу*/
        myRefrigerator.sortByExpirationDate();

        /*Вывести  содержимо холодильника после сортировки*/
        myRefrigerator.printContent();

        /*Сортировка овощей, которые есть в холодильнике, для салата */
        List<Product> vegetables = myRefrigerator.filterVegetables();
        vegetables.sort(new SortProductsByWeightComparator());
        ProductPrintHelper.printContent("Список овощей в холодильнике", vegetables);

        /*Создаем экземпляр рецепта "Авторский борщ"*/
        Receipt myBorschch = new Borsch(beef, potato, beet, carrot);

        /*Создаем экземпляр рецепта "Жареная картошка"*/
        Receipt myFriedPotato = new FriedPotato(potato);

        /*Создаем экземпляр рецепта "Винегрет"*/
        Receipt myVinegret = new Vinegret(potato, beet, carrot);

        /*Создаем экземпляр рецепта "Фрикадельки"*/
        Receipt myMeatballs = new Meatballs(beef, carrot);

        /*Добавляем рецепты в меню*/
        myMenu.setRecepies(new ArrayList<>(Arrays.asList(myBorschch, myFriedPotato, myVinegret, myMeatballs)));

        /*Считаем калорийность меню*/
        myMenu.calculateCalories();

        /*Готовим борщ*/
        /*Ловим эксепшн ExpiredProductException о просроченном ингридиенте beet */
        /*Готовим картошку*/
        /*Ловим эксепшн NotEnoughProductWeightException о недостаточном весе potato */
        try {
            myBorschch.cook();
            myFriedPotato.cook();
        } catch (ExpiredProductException | NotEnoughProductWeightException e) {
            System.out.println(e.getMessage());
        }


        /*Найти продукт в борще, который соответствует диапазону веса*/
        /*Ловим эксепшн NotEnoughProductWeightException о недостаточном весе potato */
        try {
            ProductPrintHelper.printProduct(myBorschch.findProductByWeight(0, 50));
        } catch (ProductNotFoundException e) {
            System.out.println(e.getMessage());
        }
    }
}
