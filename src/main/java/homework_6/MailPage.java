package homework_6;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.openqa.selenium.support.ui.ExpectedConditions.*;

public class MailPage extends AbstractBasePage {

    @FindBy(xpath = "//div[contains(@class,'mail-Message-Toolbar-Subject mail-Message-Toolbar-Subject_message')]")
    WebElement mailSubject;

    @FindBy(xpath = "//div[contains(@class, 'mail-Message-Body-Content')]/div")
    WebElement mailBody;

    @FindBy(css = "span.mail-Message-Sender-Email")
    WebElement mailSender;

    @FindBy(xpath = "//div[contains(@class,'ns-view-toolbar-button-delete')]")
    WebElement deleteButton;


    public MailPage(WebDriver driver) {
        super(driver);
    }

    public String getMailSubject(){
        return wait.until(visibilityOf(mailSubject)).getText();
    }

    public String getMailBody(){
        return wait.until(visibilityOf(mailBody)).getText();
    }

    public String getMailSender(){
        return wait.until(visibilityOf(mailSender)).getText();
    }

    public void deleteMail(){
        wait.until(titleContains("Письмо"));
        wait.until(elementToBeClickable(deleteButton)).click();
    }
}
