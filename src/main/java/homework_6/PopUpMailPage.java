package homework_6;

import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static org.openqa.selenium.By.cssSelector;
import static org.openqa.selenium.By.xpath;
import static org.openqa.selenium.support.ui.ExpectedConditions.*;

public class PopUpMailPage extends AbstractBasePage {

    @FindBy(xpath = "//span[@class='composeHeader-Title']/span")
    WebElement mailNewHeader;

    @FindBy(xpath = "//span[@class='composeHeader-Title']/span")
    WebElement mailDraftHeader;

    @FindBy(xpath = "//div[@class='MultipleAddressesDesktop-Field ComposeYabblesField']/div")
    WebElement mailToTextField;

    @FindBy(name = "subject")
    WebElement mailSubjectTextField;

    //@FindBy(xpath = "//div[@placeholder='Напишите что-нибудь']/div")
    @FindBy(xpath = "//div[contains(@class, 'cke_editable_themed cke_contents_ltr')]/div")
    WebElement mailBodyTextField;

    @FindBy(xpath = "//span[@class='composeHeader-SavedAt']")
    WebElement savedMailLabel;

    @FindBy(xpath = "//div[@class='ComposePopup-Content']//button[contains(@class, 'controlButtons__btn--close')]")
    WebElement closeButton;

    @FindBy(css = "span[data-email]")
    WebElement recepient;

    @FindBy(xpath = "//div[@class='ComposeSendButton-Text']/../..")
    WebElement sendButton;

    @FindBy(css = "div.ComposeDoneScreen-Title span")
    WebElement composeDoneScreen;



    public PopUpMailPage(WebDriver driver) {
        super(driver);
    }

    public String getHeaderText()
    {
        return wait.until(visibilityOf(mailNewHeader)).getText();
    }

    public void enterMailSubject(String mailSubjectText){
        wait.until(elementToBeClickable(mailSubjectTextField)).sendKeys(mailSubjectText);
    }

    public void enterMailTo(String mailToAddress){
        wait.until(elementToBeClickable(mailToTextField)).sendKeys(mailToAddress);
    }

    public void enterMailBody(String mailBodyText){
        wait.until(elementToBeClickable(mailBodyTextField)).sendKeys(mailBodyText);
    }

    public void saveDraftMail()
    {
        action.keyDown(Keys.CONTROL).sendKeys("s").build().perform();
        action.keyUp(Keys.CONTROL).sendKeys(Keys.NULL).build().perform();
    }

    public boolean isMailSaved()
    {
        return wait.until(visibilityOf(savedMailLabel)).getText().contains("сохранено");
    }

    public void closeMail()
    {
        wait.until(elementToBeClickable(closeButton)).click();
    }


    public String getMailSubject(){
        String mailSubj;
        try {
            mailSubj = wait.until(visibilityOf(mailDraftHeader)).getText();
            System.out.println(mailSubj);
        } catch (StaleElementReferenceException e){
            System.out.println("Try 2 to relocate MailDraftHeader");
            mailSubj =  wait.until(visibilityOf(mailDraftHeader)).getText();
            System.out.println(mailSubj);
        }
        return mailSubj;
    }

    public String getMailBody(){
        //sleep(5000);
        return wait.until(visibilityOf(mailBodyTextField)).getText();
    }

    public String getMailRecepient() {
        return wait.until(visibilityOf(recepient)).getAttribute("data-email");
    }

    public void sendMail()
    {
        wait.until(elementToBeClickable(sendButton)).click();
    }

    public boolean isMailSent(){
        return wait.until(visibilityOf(composeDoneScreen)).getText().contains("Письмо отправлено");

    }
}
