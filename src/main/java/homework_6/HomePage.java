package homework_6;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.openqa.selenium.By.cssSelector;
import static org.openqa.selenium.By.xpath;
import static org.openqa.selenium.support.ui.ExpectedConditions.*;

public class HomePage extends AbstractBasePage {


    @FindBy(className = "mail-ComposeButton")
    WebElement composeNewMailButton;

    @FindBy(css = "[title*='Входящие']")
    WebElement openInboxFolderButton;

    @FindBy(xpath = "//div[contains(@class,'mail-SystemFolderList')]/a[contains(@title,'Черновики')]")
    WebElement openDraftFolderButton;

    @FindBy(xpath = "//div[contains(@class,'mail-SystemFolderList')]/a[contains(@title,'Отправленные')]")
    WebElement openSentFolderButton;

    @FindBy(xpath = "//a[contains(@class, 'ns-view-user-folder') and contains(@title, 'Test')]")
    WebElement openTestFolderButton;

    @FindBy(xpath = "//div[contains(@class,'mail-SystemFolderList')]/a[contains(@title,'Удалённые')]")
    WebElement openDeletedFolderButton;

    @FindBy(xpath = "//span[contains(text(),'\" + mailSubjectText + \"')]")
    WebElement mailItemSubject;

    @FindBy(css = "div.b-messages__placeholder-item:first-child")
    WebElement messagesPlaceholderEmpty;

    @FindBy(xpath = "//a[@href=\"https://passport.yandex.ru\"]")
    WebElement accountMenu;

    @FindBy(css = "a[aria-label='Выйти из аккаунта']")
    WebElement logoutButton;

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void openInboxFolder(){
        wait.until(elementToBeClickable(openInboxFolderButton)).click();
        wait.until(titleContains("Входящие"));
    }

    public void openDeletedFolder(){
        wait.until(elementToBeClickable(openDeletedFolderButton)).click();
        wait.until(titleContains("Удалённые"));
    }

    public void openDraftFolder()
    {
//        try {

            wait.until(elementToBeClickable(openDraftFolderButton)).click();
            wait.until(titleContains("Черновики"));

//        } catch (StaleElementReferenceException e) {
//            System.out.println("openDraftFolder try 2");
//            wait.until(elementToBeClickable(openDraftFolderButton)).click();
//            wait.until(titleContains("Черновики"));
//        }
    }
    public void openSentFolder()
    {
        try {
            wait.until(elementToBeClickable(openSentFolderButton)).click();
            wait.until(titleContains("Отправленные"));
        } catch (StaleElementReferenceException e){
            System.out.println("openSentFolder try 2");
            wait.until(elementToBeClickable(openSentFolderButton)).click();
            wait.until(titleContains("Отправленные"));
        }
    }

    public void openTestFolder()
    {
        try {
            wait.until(elementToBeClickable(openTestFolderButton)).click();
            wait.until(titleContains("Test"));
        } catch (StaleElementReferenceException e){
            System.out.println("openTestFolder try 2");
            wait.until(elementToBeClickable(openTestFolderButton)).click();
            wait.until(titleContains("Test"));
        }
    }

    public String getTitle(){
        return driver.getTitle();
    }

    public void composeNewMail(){
        wait.until(elementToBeClickable(composeNewMailButton)).click();
        wait.until(visibilityOfElementLocated(cssSelector("div.composeHeader_expanded")));
    }

    public void openMailBySubject(String mailSubjectText)
    {
        try {
            //action.sendKeys(Keys.F9).build().perform();
            wait.until(visibilityOfElementLocated(xpath("//span[contains(text(),'" + mailSubjectText + "')]"))).click();
        } catch (StaleElementReferenceException e){
            wait.until(visibilityOfElementLocated(xpath("//span[contains(text(),'" + mailSubjectText + "')]"))).click();
        }
    }


    public void openMailInThread(String mailSubjectText, String mailBodyText)
    {
        try {
            wait.until(visibilityOfElementLocated(xpath("//span[contains(text(),'" + mailSubjectText + "')]"))).click();
        } catch (StaleElementReferenceException e){
            wait.until(visibilityOfElementLocated(xpath("//span[contains(text(),'" + mailSubjectText + "')]"))).click();
        }
        wait.until(presenceOfAllElementsLocatedBy(xpath("//div[contains(@class,'mail-MessageSnippet-Inner mail-MessageSnippet-Thread')]//span[contains(text(),'"+mailBodyText+"')]"))).get(1).click();
    }

    public WebElement findMailBySubject(String mailSubjectText)
    {
        //sleep(5000);
        action.sendKeys(Keys.F9).build().perform();
        return wait.until(presenceOfElementLocated(cssSelector("span[title='"+ mailSubjectText + "']")));
    }

    public boolean isFolderEmpty(){
        return wait.until(visibilityOf(messagesPlaceholderEmpty)).getText().contains("В папке «Черновики» нет писем.");
    }

    public void logout(){
        wait.until(visibilityOf(accountMenu)).click();
        wait.until(elementToBeClickable(logoutButton)).click();
        wait.until(ExpectedConditions.titleContains("Авторизация"));
    }


}
