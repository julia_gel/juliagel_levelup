package homework_6;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;

public class StartPage extends AbstractBasePage {

    @FindBy(className = "button2_theme_mail-white")
    private WebElement userLoginButton;

    public StartPage(WebDriver driver) {
        super(driver);

    }

    public void openYandexStartPage(String url){
        driver.get(url);
        wait.until(ExpectedConditions.titleContains("Яндекс.Почта — бесплатная и надежная электронная почта"));
    }

    public String getTitle(){
        return driver.getTitle();
    }

    public void clickLoginButton(){
        //sleep(3000);
        try {
            wait.until(elementToBeClickable(userLoginButton)).click();
        } catch (StaleElementReferenceException e){
            wait.until(elementToBeClickable(userLoginButton)).click();
        }

    }
}
