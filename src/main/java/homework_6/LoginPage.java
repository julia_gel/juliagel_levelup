package homework_6;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;

public class LoginPage extends AbstractBasePage {

    @FindBy(id = "passp-field-login")
    WebElement userNameTextField;

    @FindBy(id = "passp-field-passwd")
    WebElement userPasswordTextField;

    @FindBy(className = "Button2_type_submit")
    WebElement loginSubmitButton;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void login(String login, String password){
        wait.until(elementToBeClickable(userNameTextField)).sendKeys(login);
        wait.until(elementToBeClickable(loginSubmitButton)).submit();
        wait.until(elementToBeClickable(userPasswordTextField)).sendKeys(password);
        wait.until(elementToBeClickable(loginSubmitButton)).submit();
    }
}
