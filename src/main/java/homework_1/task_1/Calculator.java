package homework_1.task_1;

import homework_1.task_1.operations.*;

import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Calculator {

    public static void main(String[] args) throws IOException {

        Calculator calc = new Calculator();
        calc.startApplication();

    }

    public void startApplication() throws IOException {
        Operations operation = null;
        Scanner scanner = null;
        do {
            scanner = new Scanner(System.in);

            System.out.println("================= ");
            System.out.println("Select operation: ");
            for (Operations ops : Operations.values()) {
                System.out.println(String.format("%d: %s", ops.getNumericRepresentation(), ops));
            }

            try {
                operation = Operations.findByKey(scanner.nextInt());
                System.out.println(String.format("Selected operation %s", operation));
            } catch (InputMismatchException ime) {
                System.out.println("Unknown operation");
                scanner.next();
                continue;
            }

            try {
                Operation mathOperation = null;
                switch (operation) {
                    case PLUS:
                        mathOperation = new SummOperation();
                        break;
                    case MINUS:
                        mathOperation = new MinusOperation();
                        break;
                    case MULTIPLICATION:
                        mathOperation = new MultiplicationOperation();
                        break;
                    case POW:
                        mathOperation = new PowOperation();
                        break;
                    case FIBONACCI:
                        mathOperation = new FibonacciOperation();
                        break;
                    case FACTORIAL:
                        mathOperation = new FactorialOperation();
                        break;
                }
                if (mathOperation != null) {
                    mathOperation.makeInput(scanner);
                    mathOperation.makeCalculation();
                }
            } catch (Exception ex) {
                if (ex instanceof InputMismatchException) {
                    System.out.println("Wrong input");
                    scanner.next();
                } else if (ex instanceof IllegalArgumentException) {
                    System.out.println(String.format("Wrong input %s", ex.getLocalizedMessage()));
                }
            }
        } while (!Operations.EXIT.equals(operation));
        scanner.close();
    }
}
