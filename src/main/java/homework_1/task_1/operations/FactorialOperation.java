package homework_1.task_1.operations;

import java.util.InputMismatchException;
import java.util.Scanner;

public class FactorialOperation extends Operation<Integer, Integer> {

    @Override
    public Integer makeCalculation() {
        int result = 1;
        for (int i = 1; i <= number1; i++) {
            result *= i;
        }
        System.out.println(String.format("\nResult %d", result));
        return result;
    }

    @Override
    public void makeInput(Scanner scanner) throws InputMismatchException {
        System.out.println("Enter int value: ");
        this.setNumber1(scanner.nextInt());
    }
}
