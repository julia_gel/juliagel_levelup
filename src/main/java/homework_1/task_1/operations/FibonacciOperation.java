package homework_1.task_1.operations;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class FibonacciOperation extends Operation<Integer, java.math.BigInteger> {

    @Override
    public BigInteger makeCalculation() {
        BigInteger result;

        ArrayList<BigInteger> f = new ArrayList<>();

        f.add(BigInteger.ZERO);
        //System.out.print(String.format("%d:%s\t", 1,f.get(0)));
        f.add(BigInteger.ONE);
        //System.out.print(String.format("%d:%s\t", 2,f.get(1)));
        for (int i = 2; i < number1 + 1; i++) {
            BigInteger n = f.get(i - 1).add(f.get(i - 2));
            f.add(n);
            //System.out.print(String.format("%d:%s\t", i+1,f.get(i)));
        }

        result = f.get(number1 - 1);
        System.out.println(String.format("\nResult %s", result));
        return result;
    }

    @Override
    protected void validate(Integer inputValue) {
        if (inputValue < 0 || inputValue > 400000) {
            throw new IllegalArgumentException("Input value < 0 or > 400000");
        }
    }

    @Override
    public void makeInput(Scanner scanner) throws InputMismatchException {
        System.out.println("Enter int value: ");
        this.setNumber1(scanner.nextInt());
    }
}
