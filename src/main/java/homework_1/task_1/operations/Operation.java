package homework_1.task_1.operations;

import java.util.InputMismatchException;
import java.util.Scanner;

public abstract class Operation<V, T> {
    protected V number1;
    protected V number2;

    public void setNumber1(V number1) {
        validate(number1);
        this.number1 = number1;
    }

    public void setNumber2(V number2) {
        validate(number2);
        this.number2 = number2;
    }

    public abstract T makeCalculation();

    protected void validate(V inputValue) throws InputMismatchException {
        //do nothing
    }

    public abstract void makeInput(Scanner scanner) throws InputMismatchException;
}
