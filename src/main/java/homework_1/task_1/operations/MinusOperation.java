package homework_1.task_1.operations;

import java.util.InputMismatchException;
import java.util.Scanner;

public class MinusOperation extends Operation<Integer, Integer> {
    @Override
    public java.lang.Integer makeCalculation() {
        Integer result = number1 - number2;
        System.out.println(String.format("Result %d", result));
        return result;
    }

    @Override
    public void makeInput(Scanner scanner) throws InputMismatchException {
        System.out.println("Enter two int values: ");
        this.setNumber1(scanner.nextInt());
        this.setNumber2(scanner.nextInt());
    }
}
