package homework_1.task_1.operations;

import java.util.InputMismatchException;
import java.util.Scanner;

public class MultiplicationOperation extends Operation<Double, Double> {
    @Override
    public Double makeCalculation() {
        double result = 0;
        result = number1 * number2;
        System.out.println(String.format("\nResult %f", result));
        return result;
    }

    @Override
    public void makeInput(Scanner scanner) throws InputMismatchException {
        System.out.println("Enter two double values: ");
        this.setNumber1(scanner.nextDouble());
        this.setNumber2(scanner.nextDouble());
    }
}
