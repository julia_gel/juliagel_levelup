package homework_1.task_1.operations;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.InputMismatchException;
import java.util.Scanner;

public class PowOperation extends Operation<Integer, java.math.BigDecimal> {
    @Override
    public BigDecimal makeCalculation() {
        BigDecimal result = BigDecimal.ONE;
        BigDecimal bMinusOne = new BigDecimal("-1");
        BigDecimal bOne = new BigDecimal("1");
        BigDecimal bZero = new BigDecimal("0");

        if (number2 > 0) {
            for (int i = 1; i <= number2; i++) {
                result = result.multiply(BigDecimal.valueOf(number1));
            }
        } else if (number2 < 0) {
            for (int i = 1; i <= number2 * (-1); i++) {
                result = result.multiply(BigDecimal.valueOf(number1));
            }
            result = bOne.divide(result, 10, RoundingMode.HALF_UP);
        }
        System.out.println(String.format("\nResult (%d ^ %d) = %s", number1, number2, result));
        return result;
    }

    @Override
    public void makeInput(Scanner scanner) throws InputMismatchException {
        System.out.println("Enter two int values: ");
        this.setNumber1(scanner.nextInt());
        this.setNumber2(scanner.nextInt());
    }
}
