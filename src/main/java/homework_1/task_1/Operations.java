package homework_1.task_1;

import java.util.InputMismatchException;

public enum Operations {
    PLUS(1),
    MINUS(2),
    MULTIPLICATION(3),
    POW(4),
    FIBONACCI(5),
    FACTORIAL(6),
    EXIT(7);

    private int numericRepresentation;

    Operations(int i) {
        this.numericRepresentation = i;
    }

    public static Operations findByKey(int i) throws InputMismatchException {
        for (Operations ops : values()) {
            if (ops.numericRepresentation == i) {
                return ops;
            }
        }
        throw new InputMismatchException("Unknown number");
    }

    public int getNumericRepresentation() {
        return numericRepresentation;
    }
}
