package homework_1.task_1.operations;

import org.junit.Before;
import org.junit.Test;

import java.math.BigInteger;
import java.util.InputMismatchException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class FibonacciOperationTest {
    private FibonacciOperation testedClass;

    @Before
    public void init() {
        testedClass = new FibonacciOperation();
    }

    @Test(expected = InputMismatchException.class)
    public void test_validate() {
        testedClass.setNumber1(-1);
    }

    @Test
    public void testFibonacciCalc() {
        testedClass.setNumber1(3);
        BigInteger result = testedClass.makeCalculation();

        assertTrue(result != null);
        assertEquals(BigInteger.ONE, result);
    }
}
