package homework_5;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertEquals;

public abstract class BasicMailTest {

    protected WebDriver driver;
    protected WebDriverWait wait;
    protected Actions action;

    protected final static String URL = "https://mail.yandex.ru";
    protected final static String LOGIN = "mamma.moomin@yandex.ru";
    protected final static String PASSWORD = "LevelUp";
    protected final static String MAIL_TO = "mamma.moomin@yandex.ru";
    protected final static String MAIL_SUBJECT = "HW5 Exercise 1 ";
    protected final static String MAIL_SUBJECT_TEST = "Test ";
    protected final static String MAIL_BODY = "Let s have fun with Selenium ";
    protected String uuid;

    @BeforeMethod
    public void setUpBeforeMethod(){
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        action = new Actions(driver);
        wait = new WebDriverWait(driver, 15);
        driver.get(URL);
        driver.manage().window().maximize();
        //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        uuid = UUID.randomUUID().toString();
    }

    @AfterMethod
    public void tearDownAfterMethod(){
        driver.close();
    }

    protected void sleep(long timeout) {
        try {
            Thread.sleep(timeout);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
