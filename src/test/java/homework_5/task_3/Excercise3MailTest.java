package homework_5.task_3;

import homework_5.BasicMailTest;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import static org.openqa.selenium.By.*;
import static org.openqa.selenium.support.ui.ExpectedConditions.*;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;


public class Excercise3MailTest extends BasicMailTest {

    @Test
    public void YandexMailTask3Test() throws InterruptedException {
        //          1. Войти в почту

        /*Проверить что открытлась страница Яндекс почты*/
        String title = driver.getTitle();
        assertEquals(title, "Яндекс.Почта — бесплатная и надежная электронная почта");

        /*Нажать кнопку Войти*/
        wait.until(elementToBeClickable(className("button2_theme_mail-white"))).click();

        /*Залогиниться - ввести логин, пароль, нажать кнопку Submit*/
        wait.until(elementToBeClickable(id("passp-field-login"))).sendKeys(LOGIN);
        wait.until(elementToBeClickable(className("Button2_type_submit"))).submit();
        wait.until(elementToBeClickable(id("passp-field-passwd"))).sendKeys(PASSWORD);
        wait.until(elementToBeClickable(className("Button2_type_submit"))).submit();

        //          2. Assert, что вход выполнен успешно

        /*Открыть папку Входящие*/
        wait.until(elementToBeClickable(cssSelector("[title*='Входящие']")));
        assertTrue(driver.getTitle().contains("Входящие"));


        // 3. Создать новое письмо (заполнить адресата, тему письма и тело)

        wait.until(elementToBeClickable(className("mail-ComposeButton"))).click();
        wait.until(visibilityOfElementLocated(xpath("//span[@class='composeHeader-Title']/span"))).getText().contains("Новое письмо");
        wait.until(elementToBeClickable(xpath("//div[@class='MultipleAddressesDesktop-Field ComposeYabblesField']/div"))).sendKeys(MAIL_TO);
        wait.until(elementToBeClickable(name("subject"))).sendKeys(MAIL_SUBJECT + uuid);
        wait.until(elementToBeClickable(xpath("//div[@placeholder='Напишите что-нибудь']/div"))).sendKeys(MAIL_BODY+uuid);

        //          4. Отправить письмо

        wait.until(elementToBeClickable(xpath("//div[@class='ComposeSendButton-Text']/../.."))).click();
        wait.until(visibilityOfElementLocated(xpath("//span[contains(text(),'Письмо отправлено')]")));

        //          5. Проверить, что письмо появилось в папке Отправленные*/

        wait.until(elementToBeClickable(xpath("//div[contains(@class,'mail-SystemFolderList')]/a[contains(@title,'Отправленные')]"))).click();
        wait.until(ExpectedConditions.titleContains("Отправленные"));
        wait.until(visibilityOfElementLocated(xpath("//span[contains(text(),'" + MAIL_SUBJECT + uuid + "')]"))).click();

        //          6. Проверить, что письмо появилось в папке Входящие

        wait.until(elementToBeClickable(xpath("//a[contains(@title,'Входящие')]"))).click();
        wait.until(ExpectedConditions.titleContains("Входящие"));
        action.sendKeys(Keys.F9).build().perform();
        /*Находим тред по теме письма, кликаем на нем*/
        wait.until(visibilityOfElementLocated(xpath("//span[contains(text(),'" + MAIL_SUBJECT+uuid + "')]"))).click();
        /*Ждем, пока тред развернется*/
        wait.until(visibilityOfElementLocated(xpath("//div[contains(@class,'mail-MessageSnippet-Inner mail-MessageSnippet-Thread')]")));
        /*Ищем в треде письмо по теме*/
        wait.until(presenceOfAllElementsLocatedBy(xpath("//div[contains(@class,'mail-MessageSnippet-Inner mail-MessageSnippet-Thread')]//span[contains(text(),'"+MAIL_BODY+uuid+"')]"))).get(1).click();
        wait.until(ExpectedConditions.titleContains("Письмо"));
        wait.until(elementToBeClickable(xpath("//div[contains(@class,'ns-view-toolbar-button-delete')]"))).click();


        //          7. Проверить, что письмо появилось в папке Корзина

        wait.until(elementToBeClickable(xpath("//div[contains(@class,'mail-SystemFolderList')]/a[contains(@title,'Удалённые')]"))).click();
        wait.until(ExpectedConditions.titleContains("Удалённые"));
        wait.until(visibilityOfElementLocated(xpath("//span[contains(text(),'" + MAIL_SUBJECT + uuid + "')]"))).click();


        //          9. Разлогиниться

        wait.until(elementToBeClickable(xpath("//a[@href=\"https://passport.yandex.ru\"]"))).click();
        wait.until(elementToBeClickable(xpath("//a[@aria-label=\"Выйти из аккаунта\"]"))).click();
        wait.until(titleContains("Авторизация"));
        //sleep(5000);

    }
}
