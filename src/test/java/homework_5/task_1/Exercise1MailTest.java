package homework_5.task_1;

import homework_5.BasicMailTest;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.openqa.selenium.By.*;
import static org.openqa.selenium.support.ui.ExpectedConditions.*;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class Exercise1MailTest extends BasicMailTest {

    @Test
    public void YandexMailTask1Test() throws InterruptedException {

        // 1. Войти в почту

        /*Проверить что открытлась страница Яндекс почты*/
        String title = driver.getTitle();
        assertEquals(title, "Яндекс.Почта — бесплатная и надежная электронная почта");

        /*Нажать кнопку Войти*/
        wait.until(elementToBeClickable(className("button2_theme_mail-white"))).click();

        /*Залогиниться - ввести логин, пароль, нажать кнопку Submit*/
        wait.until(elementToBeClickable(id("passp-field-login"))).sendKeys(LOGIN);
        wait.until(elementToBeClickable(className("Button2_type_submit"))).submit();
        wait.until(elementToBeClickable(id("passp-field-passwd"))).sendKeys(PASSWORD);
        wait.until(elementToBeClickable(className("Button2_type_submit"))).submit();

        //2. Assert, что вход выполнен успешно

        /*Открыть папку Входящие*/
        wait.until(elementToBeClickable(cssSelector("[title*='Входящие']")));
        assertTrue(driver.getTitle().contains("Входящие"));

        // 3. Создать новое письмо (заполнить адресата, тему письма и тело)

        wait.until(elementToBeClickable(className("mail-ComposeButton"))).click();
        wait.until(visibilityOfElementLocated(xpath("//span[@class='composeHeader-Title']/span"))).getText().contains("Новое письмо");
        wait.until(elementToBeClickable(xpath("//div[@class='MultipleAddressesDesktop-Field ComposeYabblesField']/div"))).sendKeys(MAIL_TO);
        wait.until(elementToBeClickable(name("subject"))).sendKeys(MAIL_SUBJECT+uuid);
        wait.until(elementToBeClickable(xpath("//div[@placeholder='Напишите что-нибудь']/div"))).sendKeys(MAIL_BODY);


        // 4. Сохранить его как черновик

        action.keyDown(Keys.CONTROL).sendKeys("s").build().perform();
        wait.until(elementToBeClickable(xpath("//span[@class='composeHeader-SavedAt']"))).getText().contains("сохранено");
        action.keyUp(Keys.CONTROL).sendKeys(Keys.NULL).build().perform();
        /*Закрыть редактируемое письмо*/
        wait.until(elementToBeClickable(xpath("//div[@class='ComposePopup-Content']//button[contains(@class, 'controlButtons__btn--close')]"))).click();
        // 5. Verify, что письмо сохранено в черновиках

        wait.until(elementToBeClickable(xpath("//div[contains(@class,'mail-SystemFolderList')]/a[contains(@title,'Черновики')]"))).click();
        wait.until(ExpectedConditions.titleContains("Черновики"));

        //6. Verify контент, адресата и тему письма (должно совпадать с пунктом 3)

        /*Найти в черновиках письмо с нужной темой и открыть его*/
        wait.until(visibilityOfElementLocated(xpath("//span[contains(text(),'" + MAIL_SUBJECT+uuid + "')]"))).click();
        /*Проверить заголовок в сохраненном черновике*/
        wait.until(visibilityOfElementLocated(xpath("//span[@class='composeHeader-Title']/span"))).getText().contains(MAIL_SUBJECT+uuid);
        /*Проверить тело письма*/
        wait.until(visibilityOfElementLocated(xpath("//div[contains(@class, 'cke_editable_themed cke_contents_ltr')]/div"))).getText().contains(MAIL_BODY);
        /*Проверить отправителя*/
        wait.until(visibilityOfElementLocated(cssSelector("span[data-email='mamma.moomin@yandex.ru']")));

        // 7. Отправить письмо

        wait.until(elementToBeClickable(xpath("//div[@class='ComposeSendButton-Text']/../.."))).click();
        wait.until(visibilityOfElementLocated(xpath("//span[contains(text(),'Письмо отправлено')]")));

        // 8. Verify, что письмо пропало из черновиков
        //sleep(5000);
        wait.until(elementToBeClickable(xpath("//div[contains(@class,'mail-SystemFolderList')]/a[contains(@title,'Черновики')]"))).click();
        wait.until(ExpectedConditions.titleContains("Черновики"));
        wait.until(visibilityOfElementLocated(xpath("//div[contains(text(),'В папке «Черновики» нет писем.')]")));

        // 9. Проверить, что письмо появилось в папке Отправленные*/

        wait.until(elementToBeClickable(xpath("//div[contains(@class,'mail-SystemFolderList')]/a[contains(@title,'Отправленные')]"))).click();
        wait.until(titleContains("Отправленные"));
        wait.until(visibilityOfElementLocated(xpath("//span[contains(text(),'" + MAIL_SUBJECT+uuid + "')]"))).click();

        // 10. Разлогиниться

        wait.until(elementToBeClickable(xpath("//a[@href=\"https://passport.yandex.ru\"]"))).click();
        wait.until(elementToBeClickable(xpath("//a[@aria-label=\"Выйти из аккаунта\"]"))).click();
        wait.until(titleContains("Авторизация"));
        //sleep(5000);
    }
}
