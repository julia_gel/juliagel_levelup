package homework_8.task_1.users;

import homework_8.task_1.Endpoints;
import homework_8.task_1.BasicTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.beans.SamePropertyValuesAs.samePropertyValuesAs;
import static org.testng.Assert.assertEquals;

public class UsersGoRestTest extends BasicTest {

    @DataProvider
    public Object[][] entityDataProvider() {
        return new Object[][] {
                {new UsersRequest("Masha", "Female", "masha@testing.com", "Active"), new UserData("", "Masha", "masha@testing.com", "Female", "Active", "", "")},
                {new UsersRequest("Sasha", "Male", "sasha@testing.com", "Active"), new UserData("", "Sasha", "sasha@testing.com", "Male", "Active", "", "")},
                {new UsersRequest("Dasha", "Female", "dasha@testing.com", "Inactive"), new UserData("", "Dasha", "dasha@testing.com", "Female", "Inactive", "", "")}
        };
    }
    @Test(description = "Create Update Delete user" , dataProvider = "entityDataProvider")
    public void usersPostPutGetDeleteTest(UsersRequest request, UserData expectedUserData){
        UsersResponse actualPostResponse =
                given()
                    .spec(rqSpec)
                    .body(request)
                .when()
                    .post(Endpoints.USERS_ENDPOINT)
                .as(UsersResponse.class);

        System.out.println(actualPostResponse);
        assertThat(actualPostResponse.getCode(), equalTo(201));
        assertThat(actualPostResponse.getData(), samePropertyValuesAs(expectedUserData,  "id", "createDate", "updateDate"));

        UsersResponse actualPutResponse =
                given()
                        .spec(rqSpec)
                        .pathParam("id", actualPostResponse.getData().getId())
                        .body(request)
                        .when()
                        .put(Endpoints.USER_ENDPOINT)
                        .as(UsersResponse.class);

        System.out.println(actualPutResponse);
        assertThat(actualPutResponse.getCode(), equalTo(200));
        assertThat(actualPutResponse.getData(), samePropertyValuesAs(expectedUserData,  "id", "createDate", "updateDate"));

        UsersResponse actualGetResponse =
        given()
                .spec(rqSpec)
                .pathParam("id", actualPostResponse.getData().getId())
                .when()
                .get(Endpoints.USER_ENDPOINT)
                .as(UsersResponse.class)
        ;

        System.out.println(actualGetResponse);
        assertThat(actualGetResponse.getCode(), equalTo(200));
        assertThat(actualGetResponse.getData(), samePropertyValuesAs(expectedUserData,  "id", "createDate", "updateDate"));

        UsersResponse actualDeleteResponse =
                given()
                        .spec(rqSpec)
                        .pathParam("id", actualPostResponse.getData().getId())
                        .when()
                        .delete(Endpoints.USER_ENDPOINT)
                        .as(UsersResponse.class);

        System.out.println(actualDeleteResponse);
        assertThat(actualDeleteResponse.getCode(), equalTo(204));
    }

    @Test(description = "Get all users" )
    public void usersGetTest(){
                given()
                        .spec(rqSpec)
                        .when()
                        .get(Endpoints.USERS_ENDPOINT)
                        .then()
                .spec(rsSpec)
                .statusCode(200)
                .body("meta", notNullValue())
                .body("meta.pagination.page", equalTo(1))
                ;

    }

    @Test(description = "Get user" )
    public void userSimpleGetTest(){
        given()
                .spec(rqSpec)
                .pathParam("id", 1)
                .when()
                .get(Endpoints.USER_ENDPOINT)
                .then()
                .spec(rsSpec)
                .statusCode(200)
                .body("data.id", equalTo(1))
        ;

    }


/*    @DataProvider
    public Object[][] userDataProvider() {
        return new Object[][] {
                {new UsersRequest("Masha23", "Female", "masha23@test.com", "Active")},
        };
    }*/
/*    @Test(dataProvider = "userDataProvider")
    public void usersSimplePostTest(UsersRequest request){
                given()
                        .spec(rqSpec)
                        .body(request)
                        .when()
                        .post(Endpoints.USERS_ENDPOINT)
                .then()
                        .spec(rsSpec)
                        .body("data.id", notNullValue())
                        .body("data.name", equalTo(request.getName()))
                        .body("data.email", equalTo(request.getEmail()))
                        .body("data.gender", equalTo(request.getGender()))
                        .body("data.status", equalTo(request.getStatus()))
                        .body("data.created_at", notNullValue())
                        .body("data.updated_at", notNullValue())
                ;

    }
    @Test(dataProvider = "userDataProvider")
    public void usersSimpleDeleteTest(UsersRequest request){
        given()
                .spec(rqSpec)
                .pathParam("id", 1345)
                .when()
                .delete(Endpoints.USER_ENDPOINT)
                .then()
                .spec(rsSpec)
                .body("data.id", notNullValue())
                .body("data.name", equalTo(request.getName()))
                .body("data.email", equalTo(request.getEmail()))
                .body("data.gender", equalTo(request.getGender()))
                .body("data.status", equalTo(request.getStatus()))
                .body("data.created_at", notNullValue())
                .body("data.updated_at", notNullValue())
        ;

    }*/

}
