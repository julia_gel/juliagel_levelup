package homework_8.task_1.comments;

import homework_8.task_1.BasicTest;
import homework_8.task_1.Endpoints;
import homework_8.task_1.posts.PostData;
import homework_8.task_1.posts.PostsRequest;
import homework_8.task_1.posts.PostsResponse;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.beans.SamePropertyValuesAs.samePropertyValuesAs;

public class CommentsGoRestTest extends BasicTest {

    @DataProvider
    public Object[][] commentEntityDataProvider() {
        return new Object[][] {
                {new CommentsRequest(5, "Test user name", "test.email@test.com", "Comment body"), new CommentData(1 , 5, "Test user name", "test.email@test.com", "Comment body", "", "")},
        };
    }
    @Test(description = "Create Update Delete comment" , dataProvider = "commentEntityDataProvider")
    public void commentsPostPutGetDeleteTest(CommentsRequest request, CommentData expectedUserData){
        CommentsResponse actualPostResponse =
                given()
                    .spec(rqSpec)
                    .body(request)
                .when()
                    .post(Endpoints.COMMENTS_ENDPOINT)
                .as(CommentsResponse.class);

        System.out.println(actualPostResponse);
        assertThat(actualPostResponse.getCode(), equalTo(201));
        assertThat(actualPostResponse.getData(), samePropertyValuesAs(expectedUserData,  "id", "createDate", "updateDate"));

        CommentsResponse actualPutResponse =
                given()
                        .spec(rqSpec)
                        .pathParam("id", actualPostResponse.getData().getId())
                        .body(request)
                        .when()
                        .put(Endpoints.COMMENT_ENDPOINT)
                        .as(CommentsResponse.class);

        System.out.println(actualPutResponse);
        assertThat(actualPutResponse.getCode(), equalTo(200));
        assertThat(actualPutResponse.getData(), samePropertyValuesAs(expectedUserData,  "id", "createDate", "updateDate"));

        CommentsResponse actualGetResponse =
        given()
                .spec(rqSpec)
                .pathParam("id", actualPostResponse.getData().getId())
                .when()
                .get(Endpoints.COMMENT_ENDPOINT)
                .as(CommentsResponse.class)
        ;

        System.out.println(actualGetResponse);
        assertThat(actualGetResponse.getCode(), equalTo(200));
        assertThat(actualGetResponse.getData(), samePropertyValuesAs(expectedUserData,  "id", "createDate", "updateDate"));

        CommentsResponse actualDeleteResponse =
                given()
                        .spec(rqSpec)
                        .pathParam("id", actualPostResponse.getData().getId())
                        .when()
                        .delete(Endpoints.COMMENT_ENDPOINT)
                        .as(CommentsResponse.class);

        System.out.println(actualDeleteResponse);
        assertThat(actualDeleteResponse.getCode(), equalTo(204));
    }

    @Test(description = "Get all comments" )
    public void postsGetTest(){
                given()
                        .spec(rqSpec)
                        .when()
                        .get(Endpoints.COMMENTS_ENDPOINT)
                        .then()
                .spec(rsSpec)
                .statusCode(200)
                .body("meta", notNullValue())
                .body("meta.pagination.page", equalTo(1))
                ;

    }

    @Test(description = "Get comment" )
    public void userSimpleGetTest(){
        given()
                .spec(rqSpec)
                .pathParam("id", 1)
                .when()
                .get(Endpoints.COMMENT_ENDPOINT)
                .then()
                .spec(rsSpec)
                .statusCode(200)
                .body("data.id", equalTo(1))
        ;

    }


}
