package homework_8.task_1.posts;

import homework_8.task_1.BasicTest;
import homework_8.task_1.Endpoints;
import homework_8.task_1.users.UserData;
import homework_8.task_1.users.UsersRequest;
import homework_8.task_1.users.UsersResponse;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.beans.SamePropertyValuesAs.samePropertyValuesAs;

public class PostsGoRestTest extends BasicTest {

    @DataProvider
    public Object[][] postEntityDataProvider() {
        return new Object[][] {
                {new PostsRequest(1805, "Post title", "Post body"), new PostData(1 , 1805, "Post title", "Post body", "", "")},
        };
    }
    @Test(description = "Create Update Delete post" , dataProvider = "postEntityDataProvider")
    public void postsPostPutGetDeleteTest(PostsRequest request, PostData expectedUserData){
        PostsResponse actualPostResponse =
                given()
                    .spec(rqSpec)
                    .body(request)
                .when()
                    .post(Endpoints.POSTS_ENDPOINT)
                .as(PostsResponse.class);

        System.out.println(actualPostResponse);
        assertThat(actualPostResponse.getCode(), equalTo(201));
        assertThat(actualPostResponse.getData(), samePropertyValuesAs(expectedUserData,  "id", "createDate", "updateDate"));

        PostsResponse actualPutResponse =
                given()
                        .spec(rqSpec)
                        .pathParam("id", actualPostResponse.getData().getId())
                        .body(request)
                        .when()
                        .put(Endpoints.POST_ENDPOINT)
                        .as(PostsResponse.class);

        System.out.println(actualPutResponse);
        assertThat(actualPutResponse.getCode(), equalTo(200));
        assertThat(actualPutResponse.getData(), samePropertyValuesAs(expectedUserData,  "id", "createDate", "updateDate"));

        PostsResponse actualGetResponse =
        given()
                .spec(rqSpec)
                .pathParam("id", actualPostResponse.getData().getId())
                .when()
                .get(Endpoints.POST_ENDPOINT)
                .as(PostsResponse.class)
        ;

        System.out.println(actualGetResponse);
        assertThat(actualGetResponse.getCode(), equalTo(200));
        assertThat(actualGetResponse.getData(), samePropertyValuesAs(expectedUserData,  "id", "createDate", "updateDate"));

        PostsResponse actualDeleteResponse =
                given()
                        .spec(rqSpec)
                        .pathParam("id", actualPostResponse.getData().getId())
                        .when()
                        .delete(Endpoints.POST_ENDPOINT)
                        .as(PostsResponse.class);

        System.out.println(actualDeleteResponse);
        assertThat(actualDeleteResponse.getCode(), equalTo(204));
    }

    @Test(description = "Get all posts" )
    public void postsGetTest(){
                given()
                        .spec(rqSpec)
                        .when()
                        .get(Endpoints.POSTS_ENDPOINT)
                        .then()
                .spec(rsSpec)
                .statusCode(200)
                .body("meta", notNullValue())
                .body("meta.pagination.page", equalTo(1))
                ;

    }

    @Test(description = "Get post" )
    public void userSimpleGetTest(){
        given()
                .spec(rqSpec)
                .pathParam("id", 1)
                .when()
                .get(Endpoints.POST_ENDPOINT)
                .then()
                .spec(rsSpec)
                .statusCode(200)
                .body("data.id", equalTo(1))
        ;

    }


}
