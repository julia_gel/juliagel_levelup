package homework_8.task_1;

public  final class Endpoints {

    public static final String BASE_URL = "https://gorest.co.in/public-api";
    public static final String USER_ENDPOINT = "/users/{id}";
    public static final String USERS_ENDPOINT = "/users";
    public static final String POST_ENDPOINT = "/posts/{id}";
    public static final String POSTS_ENDPOINT = "/posts";
    public static final String COMMENT_ENDPOINT = "/comments/{id}";
    public static final String COMMENTS_ENDPOINT = "/comments";

    private Endpoints() {}
}
