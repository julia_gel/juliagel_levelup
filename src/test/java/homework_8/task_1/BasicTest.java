package homework_8.task_1;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.testng.annotations.BeforeMethod;

public abstract class BasicTest {
    public RequestSpecification rqSpec;
    public ResponseSpecification rsSpec;

    @BeforeMethod
    public void setUp() {

        rqSpec = new RequestSpecBuilder()
                .setBaseUri(Endpoints.BASE_URL)
                .addHeader("Authorization", "Bearer 00b17339f55f47d77f3c66d6411ca4129bf429ef6ce85839177005da41fdeabe")
                .log(LogDetail.ALL)
                .setContentType(ContentType.JSON)
                .build();
        rsSpec = new ResponseSpecBuilder()
                .expectStatusCode(200)
                .expectContentType(ContentType.JSON)
                .log(LogDetail.ALL)
                .build();

    }
}
