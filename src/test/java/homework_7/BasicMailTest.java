package homework_7;

import com.codeborne.selenide.Browsers;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.logevents.SelenideLogger;
import homework_7.util.PropertyReaderUtil;
import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.selenide.AllureSelenide;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import java.util.UUID;

public abstract class BasicMailTest {


    protected final static String URL = PropertyReaderUtil.getConfigProperties("url");
    protected final static String LOGIN = PropertyReaderUtil.getConfigProperties("user.login");
    protected final static String ACCOUNT = PropertyReaderUtil.getConfigProperties("user.account");
    protected final static String PASSWORD = PropertyReaderUtil.getConfigProperties("user.password");
    protected final static String MAIL_TO = PropertyReaderUtil.getConfigProperties("user.login");
    protected final static String MAIL_SUBJECT = "HW7 ";
    protected final static String MAIL_SUBJECT_TEST = "Test ";
    protected final static String MAIL_BODY = "Let s have fun with Selenium ";
    protected String uuid;

    @BeforeSuite
    public void beforeSuite(){
        SelenideLogger.addListener("AllureSelenide", new AllureSelenide().savePageSource(true).screenshots(true));
    }
    @BeforeMethod
    public void setUpBeforeMethod(){

        Configuration.browser = Browsers.CHROME;
        Configuration.timeout = 15000;
        Configuration.reportsFolder = "target/reports/tests";
        Configuration.startMaximized = false;
        Configuration.headless = true;

        uuid = UUID.randomUUID().toString();
    }

    @AfterMethod
    public void tearDownAfterMethod(){

        Selenide.closeWindow();
    }

    protected void sleep(long timeout) {
        try {
            Thread.sleep(timeout);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
