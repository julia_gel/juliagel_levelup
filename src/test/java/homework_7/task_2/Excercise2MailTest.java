package homework_7.task_2;

import homework_7.*;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;


public class Excercise2MailTest extends BasicMailTest {

    @Test
    public void YandexMailTask2Test() throws InterruptedException {
        //          1. Войти в почту
        StartPage startPage = new StartPage();
        startPage.openYandexStartPage(URL);

        startPage.clickLoginButton();

        LoginPage loginPage = new LoginPage();
        loginPage.login(LOGIN, PASSWORD);

        //          2. Assert, что вход выполнен успешно
        HomePage homePage = new HomePage();
        homePage.openInboxFolder();
        assertEquals(homePage.getLoggedInUserAccount(), ACCOUNT, "Failed to login'");

        // 3. Создать новое письмо (заполнить адресата, тему письма и тело)
        homePage.composeNewMail();

        PopUpMailPage popUpMailPage = new PopUpMailPage();
        popUpMailPage.enterMailTo(MAIL_TO);
        popUpMailPage.enterMailSubject(MAIL_SUBJECT_TEST+uuid);
        popUpMailPage.enterMailBody(MAIL_BODY+uuid);

        //          4. Отправить письмо
        popUpMailPage.sendMail();
        assertTrue(popUpMailPage.isMailSent(), "Mail send");

        //          5. Проверить, что письмо появилось в папке Отправленные
        homePage.openSentFolder();
        homePage.openMailBySubject(MAIL_SUBJECT_TEST+uuid);

        //          6. Проверить, что письмо появилось в папке Test
        homePage.openTestFolder();
        homePage.openMailInThread(MAIL_SUBJECT_TEST+uuid,MAIL_BODY+uuid );

        //          7. Verify контент, адресата и тему письма (должно совпадать с пунктом 3)
        MailPage mailPage = new MailPage();
        assertEquals(mailPage.getMailSubject(), MAIL_SUBJECT_TEST+uuid);
        assertEquals(mailPage.getMailBody(),MAIL_BODY+uuid);
        assertEquals(mailPage.getMailSender(), MAIL_TO);

        //          8. Разлогиниться
        homePage.logout();
//        sleep(5000);*/

    }
}
