package homework_7.task_1;

import homework_7.*;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class Exercise1MailTest extends BasicMailTest {


    @Test
    public void YandexMailTask1Test() throws InterruptedException {

        // 1. Войти в почту
        StartPage startPage = new StartPage();
        startPage.openYandexStartPage(URL);

        startPage.clickLoginButton();

        LoginPage loginPage = new LoginPage();
        loginPage.login(LOGIN, PASSWORD);

        //2. Assert, что вход выполнен успешно
        HomePage homePage = new HomePage();
        homePage.openInboxFolder();
        assertEquals(homePage.getLoggedInUserAccount(), ACCOUNT, "Failed to login'");

        // 3. Создать новое письмо (заполнить адресата, тему письма и тело)
        homePage.composeNewMail();

        PopUpMailPage popUpMailPage = new PopUpMailPage();
        popUpMailPage.enterMailTo(MAIL_TO);
        popUpMailPage.enterMailSubject(MAIL_SUBJECT+uuid);
        popUpMailPage.enterMailBody(MAIL_BODY);

        // 4. Сохранить его как черновик
        popUpMailPage.saveDraftMail();

        // 5. Verify, что письмо сохранено в черновиках
        assertTrue(popUpMailPage.isMailSaved().contains("сохранено"), "Mail saved as a draft failed");
        popUpMailPage.closeMail();

        //6. Verify контент, адресата и тему письма (должно совпадать с пунктом 3)
        homePage.openDraftFolder();
        homePage.openMailBySubject(MAIL_SUBJECT+uuid);
        //sleep(5000);
        assertTrue(popUpMailPage.getMailBody().contains(MAIL_BODY.trim()), "Mail body check failed");
        assertEquals(popUpMailPage.getMailRecepient(),LOGIN, "Mail recepient check failed");
        assertEquals(popUpMailPage.getMailSubject(), MAIL_SUBJECT+uuid, "Mail subject check failed");

        // 7. Отправить письмо
        popUpMailPage.sendMail();
        assertTrue(popUpMailPage.isMailSent(), "Mail send");

        // 8. Verify, что письмо пропало из черновиков
        homePage.openInboxFolder();
        homePage.findMailBySubject(MAIL_SUBJECT+uuid);
        homePage.openDraftFolder();
        assertTrue(homePage.isFolderEmpty().contains("В папке «Черновики» нет писем."));

        // 9. Проверить, что письмо появилось в папке Отправленные
        homePage.openSentFolder();
        homePage.openMailBySubject(MAIL_SUBJECT+uuid);

        // 10. Разлогиниться
        homePage.logout();
    }
}
