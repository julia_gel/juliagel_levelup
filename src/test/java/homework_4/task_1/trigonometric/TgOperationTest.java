package homework_4.task_1.trigonometric;

import homework_4.task_1.BasicCalculatorTest;
import homework_4.task_1.TestDataProvider;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TgOperationTest extends BasicCalculatorTest {
    @Test(testName = "double tg operation test", dataProviderClass = TestDataProvider.class, dataProvider = "tgDoubleDataProvider")
    public void tgOperationDoubleTest(double a, double expectation){
        double result = calculator.tg(a);
        Assert.assertEquals(result, expectation);
    }
}
