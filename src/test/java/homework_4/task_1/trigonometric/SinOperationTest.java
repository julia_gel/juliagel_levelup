package homework_4.task_1.trigonometric;

import homework_4.task_1.BasicCalculatorTest;
import homework_4.task_1.TestDataProvider;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SinOperationTest extends BasicCalculatorTest {
    @Test(testName = "double sin operation test", dataProviderClass = TestDataProvider.class, dataProvider = "sinDoubleRadiansDataProvider")
    public void sinOperationDoubleTest(double a, double expectation){
        double result = calculator.sin(a);
        Assert.assertEquals(result, expectation, 0.00000001);
    }
}
