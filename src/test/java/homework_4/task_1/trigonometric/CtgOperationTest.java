package homework_4.task_1.trigonometric;

import homework_4.task_1.BasicCalculatorTest;
import homework_4.task_1.TestDataProvider;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CtgOperationTest extends BasicCalculatorTest {
    @Test(testName = "double ctg operation test", dataProviderClass = TestDataProvider.class, dataProvider = "ctgDoubleDataProvider")
    public void ctgOperationDoubleTest(double a, double expectation){
        double result = calculator.ctg(a);
        Assert.assertEquals(result, expectation);
    }
}
