package homework_4.task_1.trigonometric;

import homework_4.task_1.BasicCalculatorTest;
import homework_4.task_1.TestDataProvider;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CosOperationTest extends BasicCalculatorTest {
    @Test(testName = "double cos operation test", dataProviderClass = TestDataProvider.class, dataProvider = "cosDoubleRadiansDataProvider")
    public void cosOperationDoubleTest(double a, double expectation){
        double result = calculator.cos(a);
        Assert.assertEquals(result, expectation, 0.00000001);
    }
}
