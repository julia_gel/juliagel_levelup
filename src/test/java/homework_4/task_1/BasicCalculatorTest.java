package homework_4.task_1;

import com.epam.tat.module4.Calculator;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public abstract class BasicCalculatorTest {

    protected Calculator calculator;
    @BeforeMethod
    public void setupBeforeMethod(){
        System.out.println("setupBeforeMethod");
        calculator = new Calculator();
    }

    @AfterMethod
    public void tearDownAfterMethod(){
        System.out.println("tearDownMethod");
        calculator = null;
    }

}
