package homework_4.task_1.sign;

import homework_4.task_1.BasicCalculatorTest;
import homework_4.task_1.TestDataProvider;
import org.testng.Assert;
import org.testng.annotations.Test;

public class IsNegativeOperationTest extends BasicCalculatorTest {

    @Test(testName = "long IsNegativeOperationTest operation test", dataProviderClass = TestDataProvider.class, dataProvider = "isNegativeLongDataProvider")
    public void isNegativeOperationLongTest(long a, boolean expectation){
        boolean result = calculator.isNegative(a);
        Assert.assertEquals(result, expectation);
    }
}
