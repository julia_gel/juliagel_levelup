package homework_4.task_1.sign;

import homework_4.task_1.BasicCalculatorTest;
import homework_4.task_1.TestDataProvider;
import org.testng.Assert;
import org.testng.annotations.Test;

public class IsPositiveOperationTest extends BasicCalculatorTest {

    @Test(testName = "long isPositive operation test", dataProviderClass = TestDataProvider.class, dataProvider = "isPositiveLongDataProvider")
    public void isPositiveOperationLongTest(long a, boolean expectation){
        boolean result = calculator.isPositive(a);
        Assert.assertEquals(result, expectation);
    }
}
