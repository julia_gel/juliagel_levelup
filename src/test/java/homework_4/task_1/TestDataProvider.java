package homework_4.task_1;

import org.testng.annotations.DataProvider;

public class TestDataProvider {

    @DataProvider
    public Object[][] sumLongDataProvider(){
        return new Object[][] {
                { 2, 2, 4},
                { 0, 0, 0},
                {-1, 1, 0}
        };
    }

    @DataProvider
    public Object[][] sumDoubleDataProvider(){
        return new Object[][] {
                { 2.2, 2.2, 4.4},
                { 0, 0, 0},
                {0.9, -0.9, 0}
        };
    }

    @DataProvider
    public Object[][] subLongDataProvider(){
        return new Object[][] {
                { 2, -2, 4},
                { -2, -2, 0},
                {-1, 1, -2}
        };
    }

    @DataProvider
    public Object[][] subDoubleDataProvider(){
        return new Object[][] {
                { 2.0, -2.0, 4.0},
                { -2.2, -2.2, 0},
                {-1.1, 1.1, -2.2}
        };
    }

    @DataProvider
    public Object[][] multLongDataProvider(){
        return new Object[][] {
                { 2, 2, 4},
                { 0, 0, 0},
                {-1, 1, -1}
        };
    }

    @DataProvider
    public Object[][] multDoubleDataProvider(){
        return new Object[][] {
                { 2.2, 2.2, 4.84},
                { 0, 0, 0},
                {0.9, -0.9, -0.81}
        };
    }
    @DataProvider
    public Object[][] divLongDataProvider(){
        return new Object[][] {
                { 2, 2, 1},
                {0, 1, 0}
        };
    }
    @DataProvider
    public Object[][] divLongExceptionDataProvider(){
        return new Object[][] {
                {0, 0, 0},
                {1,0,0},
                {1,0L,0}
        };
    }

    @DataProvider
    public Object[][] divDoubleDataProvider(){
        return new Object[][] {
                { 4, 2, 2},
                { 4.2, 2.2, 1.909090909090909},
                {0.9, -0.9, -1},
                {1, 0, Double.POSITIVE_INFINITY}
        };
    }

    @DataProvider
    public Object[][] powDoubleDataProvider(){
        return new Object[][] {
                { 3, 3, 27},
                { 4, 0, 1},
                {4, 1, 4},
                {-3, 3, -27},
                {3, -3, 0.037037037037037},
                {1, -4, 1}
        };
    }

    @DataProvider
    public Object[][] sqrtDoubleDataProvider(){
        return new Object[][] {
                { 4, 2},
                { 0, 0},
                {0.09, 0.3},
                {10, 3.162277660168379},
                {-16, 4}
        };
    }

    @DataProvider
    public Object[][] tgDoubleDataProvider(){
        return new Object[][] {
                {45, 1},
                {135, 1},
                {225, 1},
                {315, 1},
                {0, 0},
                {90, Double.NaN},
                {180, 0},
                {270, Double.NaN},
                {360, 0}
        };
    }

    @DataProvider
    public Object[][] ctgDoubleDataProvider(){
        return new Object[][] {
                {45, 1},
                {135, -1},
                {225, 1},
                {315, -1},
                {0, Double.NaN},
                {90, 0},
                {180, Double.NaN},
                {270, 0},
                {360, Double.NaN}
        };
    }

    @DataProvider
    public Object[][] cosDoubleDegreesDataProvider(){
        return new Object[][] {
                {45, 0.7071},
                {135, 0.7071},
                {225, -0.7071},
                {315, -0.7071},
                {0, 0},
                {90, 0},
                {180, 0},
                {270, -1},
                {360, 0}
        };
    }

    @DataProvider
    public Object[][] cosDoubleRadiansDataProvider(){
        return new Object[][] {
                {0.78539816, 0.7071067787841887},
                {2.3561944901, 0.7071067787841887},
                {3.926990816, -0.7071067787841887},
                {5.4977871437, -0.7071067787841887},
                {0, 0},
                {1.5707963, 0},
                {3.141592653589, 0},
                {4.7123889803, -1}
        };
    }

    @DataProvider
    public Object[][] sinDoubleRadiansDataProvider(){
        return new Object[][] {
                {0.78539816, 0.7071067787841887}, //45
                {2.3561944901, 0.7071067787841887}, //135
                {3.926990816, -0.7071067787841887}, //225
                {5.4977871437, -0.7071067787841887}, // 315
                {0, 0}, //0
                {1.5707963, 1}, //90
                {3.141592653589, 0}, //180
                {4.7123889803, -1}, //270
                {0, 0} //360
        };
    }

    @DataProvider
    public Object[][] isPositiveLongDataProvider(){
        return new Object[][] {
                {0, false},
                {1, true},
                {-1, false}
        };
    }

    @DataProvider
    public Object[][] isNegativeLongDataProvider(){
        return new Object[][] {
                {0, false},
                {1, false},
                {-1, true}
        };
    }

}
