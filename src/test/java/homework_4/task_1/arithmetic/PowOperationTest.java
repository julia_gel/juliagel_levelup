package homework_4.task_1.arithmetic;

import homework_4.task_1.BasicCalculatorTest;
import homework_4.task_1.TestDataProvider;
import org.testng.Assert;
import org.testng.annotations.Test;

public class PowOperationTest extends BasicCalculatorTest {
    @Test(testName = "double pow operation test", dataProviderClass = TestDataProvider.class, dataProvider = "powDoubleDataProvider")
    public void powOperationDoubleTest(double a, double b, double expectation){
        double result = calculator.pow(a,b);
        Assert.assertEquals(result, expectation, 0.00000001);
    }
}
