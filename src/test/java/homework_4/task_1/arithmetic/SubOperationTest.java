package homework_4.task_1.arithmetic;

import homework_4.task_1.BasicCalculatorTest;
import homework_4.task_1.TestDataProvider;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SubOperationTest extends BasicCalculatorTest {
    @Test(testName = "long sub operation test", dataProviderClass = TestDataProvider.class, dataProvider = "subLongDataProvider")
    public void subOperationLongTest(long a, long b, long expectation){
        long result = calculator.sub(a,b);
        Assert.assertEquals(result, expectation);
    }

    @Test(testName = "double sub operation test", dataProviderClass = TestDataProvider.class, dataProvider = "subDoubleDataProvider")
    public void subOperationDoubleTest(double a, double b, double expectation){
        double result = calculator.sub(a,b);
        Assert.assertEquals(result, expectation);
    }
}
