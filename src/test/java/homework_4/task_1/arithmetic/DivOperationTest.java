package homework_4.task_1.arithmetic;

import homework_4.task_1.BasicCalculatorTest;
import homework_4.task_1.TestDataProvider;
import org.testng.Assert;
import org.testng.annotations.Test;

public class DivOperationTest extends BasicCalculatorTest {

    @Test(testName = "long div operation test", dataProviderClass = TestDataProvider.class, dataProvider = "divLongDataProvider")
    public void divOperationLongTest(long a, long b, long expectation){
        long result = calculator.div(a,b);
        Assert.assertEquals(result, expectation);
    }

    @Test(testName = "long div operation test", dataProviderClass = TestDataProvider.class, dataProvider = "divLongExceptionDataProvider", expectedExceptions = NumberFormatException.class)
    public void divOperationLongExceptionTest(long a, long b, long expectation){
        long result = calculator.div(a,b);
        Assert.assertEquals(result, expectation);
    }

    @Test(testName = "double div operation test", dataProviderClass = TestDataProvider.class, dataProvider = "divDoubleDataProvider")
    public void divOperationDoubleTest(double a, double b, double expectation){
        double result = calculator.div(a,b);
        Assert.assertEquals(result, expectation);
    }
}
