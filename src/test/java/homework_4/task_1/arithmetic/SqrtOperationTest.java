package homework_4.task_1.arithmetic;

import homework_4.task_1.BasicCalculatorTest;
import homework_4.task_1.TestDataProvider;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SqrtOperationTest extends BasicCalculatorTest {
    @Test(testName = "double sqrt operation test", dataProviderClass = TestDataProvider.class, dataProvider = "sqrtDoubleDataProvider")
    public void powOperationDoubleTest(double a, double expectation){
        double result = calculator.sqrt(a);
        Assert.assertEquals(result, expectation, 0.00000001);
    }
}
