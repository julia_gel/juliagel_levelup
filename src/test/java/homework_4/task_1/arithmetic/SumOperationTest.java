package homework_4.task_1.arithmetic;

import homework_4.task_1.BasicCalculatorTest;
import homework_4.task_1.TestDataProvider;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SumOperationTest extends BasicCalculatorTest {
    @Test(testName = "long sum operation test", dataProviderClass = TestDataProvider.class, dataProvider = "sumLongDataProvider")
    public void sumOperationLongTest(long a, long b, long expectation){
        System.out.println("sumOperationLongTest");
        long result = calculator.sum(a,b);
        Assert.assertEquals(result, expectation);
    }
    @Test(testName = "double sum operation test", dataProviderClass = TestDataProvider.class, dataProvider = "sumDoubleDataProvider")
    public void sumOperationDoubleTest(double a, double b, double expectation){
        System.out.println("sumOperationDoubleTest");
        double result = calculator.sum(a,b);
        Assert.assertEquals(result, expectation);
    }
}
