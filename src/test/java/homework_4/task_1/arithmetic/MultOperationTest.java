package homework_4.task_1.arithmetic;

import homework_4.task_1.BasicCalculatorTest;
import homework_4.task_1.TestDataProvider;
import org.testng.Assert;
import org.testng.annotations.Test;

public class MultOperationTest extends BasicCalculatorTest {
    @Test(testName = "long mult operation test", dataProviderClass = TestDataProvider.class, dataProvider = "multLongDataProvider")
    public void multOperationLongTest(long a, long b, long expectation){
        long result = calculator.mult(a,b);
        Assert.assertEquals(result, expectation);
    }
    @Test(testName = "double mult operation test", dataProviderClass = TestDataProvider.class, dataProvider = "multDoubleDataProvider")
    public void multOperationDoubleTest(double a, double b, double expectation){
        double result = calculator.mult(a,b);
        Assert.assertEquals(result, expectation);
    }
}
