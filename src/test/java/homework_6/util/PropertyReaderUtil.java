package homework_6.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyReaderUtil {
    Properties prop = new Properties();

    public static String getConfigProperties(String key)
    {
        Properties prop = new Properties();
        try{
            InputStream input = new FileInputStream("src/main/resources/homework_6/config.properties");
            prop.load(input);
            return prop.getProperty(key);
        } catch (IOException ex){
            ex.printStackTrace();
            throw new IllegalStateException();
        }
    }

}
