package homework_6;

import homework_6.util.PropertyReaderUtil;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.util.UUID;

public abstract class BasicMailTest {

    protected WebDriver driver;
    protected WebDriverWait wait;
    protected Actions action;

    protected final static String URL = PropertyReaderUtil.getConfigProperties("url");
    protected final static String LOGIN = PropertyReaderUtil.getConfigProperties("user.login");
    protected final static String PASSWORD = PropertyReaderUtil.getConfigProperties("user.password");
    protected final static String MAIL_TO = PropertyReaderUtil.getConfigProperties("user.login");
    protected final static String MAIL_SUBJECT = "HW6 ";
    protected final static String MAIL_SUBJECT_TEST = "Test ";
    protected final static String MAIL_BODY = "Let s have fun with Selenium ";
    protected String uuid;

    @BeforeMethod
    public void setUpBeforeMethod(){
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        //action = new Actions(driver);
        //wait = new WebDriverWait(driver, 15);
        //driver.get(URL);
        driver.manage().window().maximize();
        //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        uuid = UUID.randomUUID().toString();
    }

    @AfterMethod
    public void tearDownAfterMethod(){
        driver.close();
    }

    protected void sleep(long timeout) {
        try {
            Thread.sleep(timeout);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
