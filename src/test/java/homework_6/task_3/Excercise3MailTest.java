package homework_6.task_3;

import homework_6.BasicMailTest;
import homework_6.*;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import static org.openqa.selenium.By.*;
import static org.openqa.selenium.support.ui.ExpectedConditions.*;
import static org.testng.Assert.*;


public class Excercise3MailTest extends BasicMailTest {


    @Test
    public void YandexMailTask3Test() throws InterruptedException {
        //          1. Войти в почту
        StartPage startPage = new StartPage(driver);
        startPage.openYandexStartPage(URL);
        startPage.clickLoginButton();
        LoginPage loginPage = new LoginPage(driver);
        loginPage.login(LOGIN, PASSWORD);

        //          2. Assert, что вход выполнен успешно
        HomePage homePage = new HomePage(driver);
        homePage.openInboxFolder();
        assertTrue(homePage.getTitle().contains("Входящие"), "Failed to open 'Входящие'");


        // 3. Создать новое письмо (заполнить адресата, тему письма и тело)
        homePage.composeNewMail();
        PopUpMailPage popUpMailPage = new PopUpMailPage(driver);
        popUpMailPage.enterMailTo(MAIL_TO);
        popUpMailPage.enterMailSubject(MAIL_SUBJECT+uuid);
        popUpMailPage.enterMailBody(MAIL_BODY+uuid);

        //          4. Отправить письмо
        popUpMailPage.sendMail();
        assertTrue(popUpMailPage.isMailSent(), "Mail send");

        //          5. Проверить, что письмо появилось в папке Отправленные*/
        homePage.openSentFolder();
        homePage.findMailBySubject(MAIL_SUBJECT+uuid);

        //          6. Проверить, что письмо появилось в папке Входящие
        homePage.openInboxFolder();
        homePage.openMailInThread(MAIL_SUBJECT+uuid,MAIL_BODY+uuid );
        MailPage mailPage = new MailPage(driver);
        mailPage.deleteMail();

        //          7. Проверить, что письмо появилось в папке Корзина
        homePage.openDeletedFolder();
        homePage.findMailBySubject(MAIL_SUBJECT+uuid);

        //          9. Разлогиниться
        homePage.logout();
        //sleep(5000);

    }
}
