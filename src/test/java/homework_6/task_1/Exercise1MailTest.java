package homework_6.task_1;

import homework_6.BasicMailTest;
import homework_6.HomePage;
import homework_6.LoginPage;
import homework_6.PopUpMailPage;
import homework_6.StartPage;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import static org.openqa.selenium.By.*;
import static org.openqa.selenium.support.ui.ExpectedConditions.*;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class Exercise1MailTest extends BasicMailTest {


    @Test
    public void YandexMailTask1Test() throws InterruptedException {

        // 1. Войти в почту
        StartPage startPage = new StartPage(driver);
        startPage.openYandexStartPage(URL);
        startPage.clickLoginButton();

        LoginPage loginPage = new LoginPage(driver);
        loginPage.login(LOGIN, PASSWORD);

        //2. Assert, что вход выполнен успешно
        HomePage homePage = new HomePage(driver);
        homePage.openInboxFolder();
        assertTrue(homePage.getTitle().contains("Входящие"), "Failed to open 'Входящие'");

        // 3. Создать новое письмо (заполнить адресата, тему письма и тело)
        homePage.composeNewMail();
        PopUpMailPage popUpMailPage = new PopUpMailPage(driver);
        popUpMailPage.enterMailTo(MAIL_TO);
        popUpMailPage.enterMailSubject(MAIL_SUBJECT+uuid);
        popUpMailPage.enterMailBody(MAIL_BODY);

        // 4. Сохранить его как черновик
        popUpMailPage.saveDraftMail();
        assertTrue(popUpMailPage.isMailSaved(), "Mail saved as a draft failed");
        popUpMailPage.closeMail();

        // 5. Verify, что письмо сохранено в черновиках
        homePage.openDraftFolder();

        //6. Verify контент, адресата и тему письма (должно совпадать с пунктом 3)
        homePage.openMailBySubject(MAIL_SUBJECT+uuid);
        assertTrue(popUpMailPage.getMailBody().contains(MAIL_BODY.trim()), "Mail body check failed");
        assertEquals(popUpMailPage.getMailRecepient(),LOGIN, "Mail recepient check failed");
        assertEquals(popUpMailPage.getMailSubject(), MAIL_SUBJECT+uuid, "Mail subject check failed");

        // 7. Отправить письмо
        popUpMailPage.sendMail();
        assertTrue(popUpMailPage.isMailSent(), "Mail send");

        // 8. Verify, что письмо пропало из черновиков
        homePage.openInboxFolder();
        homePage.findMailBySubject(MAIL_SUBJECT+uuid);
        homePage.openDraftFolder();
        homePage.isFolderEmpty();

        // 9. Проверить, что письмо появилось в папке Отправленные*/
        homePage.openSentFolder();
        homePage.openMailBySubject(MAIL_SUBJECT+uuid);

        // 10. Разлогиниться
        homePage.logout();
    }
}
